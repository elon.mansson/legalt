👏 Lagalt - store and share Projects
This is an application to share projects with other people.

✌️ Description

🔥 About the application
This application is the backend part and it creates APIs for a frontend application to use. This application could be described as a redditclone. You can
post projects with title,description, photos and much more. You can apply to join the project if you want to help the creater of the project. There is a 
message board to ask or talk to others in the project. The use for it is to use it as a website to store and share projects.

For this project you will have to setup following environments:

PostgreSQL (E.g. PgAdmin)

What we used:

Backend: IntelliJ

Frontend: React
Repo for frontend:https://gitlab.com/elon.mansson/keycloak

PostgreSQL (E.g. PgAdmin)



🔥 Application endpoints

User API:

* **GET**: All users ```/api/v1/users```
* **GET**: User by uid ```/api/v1/users/{uid}```
* **GET**: The user projects ```/api/v1/users/{uid}/projects```
* **GET**: The user skills ```/api/v1/users/{uid}/skills```
* **GET**: The user project which the user is admin in ```/api/v1/users/{uid}/adminprojects```
* **GET**: The user history  ```/api/v1/users/{uid}/historyprojects```
* **GET**: The user accomplishments  ```/api/v1/users/{uid}/accomplishments```
* **GET**: The user profile data  ```/api/v1/users/{uid}/profilePage```
* **POST**: Create user with token```/api/v1/users/register```
* **PUT**: Update user```/api/v1/users/{uid}```
* **PUT**: add/update skill to the user```/api/v1/users/{uid}/skills/{id}```
* **DELETE**: Delete user```/api/v1/users/{uid}```

Project API:
* **GET**: All projects ```api/v1/projects```
* **GET**: Users on project by id ```api/v1/projects/{id}/users```
* **GET**: Admin on project by id ```api/v1/projects/{id}/admin```
* **POST**: Create project and adds user by body ```api/v1/projects```
* **PUT**: Update admin on project ```api/v1/projects/{id}/admin```
* **PUT**: Add user to project```api/v1/projects/{id}/adduser```
* **PUT**: removes user to project```api/v1/projects/{id}/removeuser```
* **PUT**: Add request to join project```api/v1/projects/{id}/addrequest```
* **PUT**: Accepts the request```api/v1/projects/{id}/handlerequest```
* **PUT**: Adds history to user```api/v1/projects/{id}/addhistory```
* **Delete**: User to project```api/v1/projects/{id}```

Skills API:
* **GET**: All skills ```api/v1/skills```
* **GET**: Skill by id ```api/v1/skills/{id}```
* **POST**: Add skill to skill table and to user by uid ```api/v1/skills/{uid}```
* **PUT**: Update skill by id ```api/v1/skills/{id}```
* **DELETE**: Skill by id ```api/v1/skills/{id}```

Message API:
* **GET**: All messages ```api/v1/message```
* **GET**: Message by id ```api/v1/message/{id}```
* **POST**: Create message ```api/v1/message/{id}```
* **PUT**: Update message by id ```api/v1/message/{id}```
* **DELETE**: Message by id ```api/v1/message/{id}```

Photo API:
* **POST**: Create a photo ```api/v1/photos```



🤖 Technologies
Tools and technologies used in this project.

Java
Spring Boot
Spring Web
Spring Data JPA
PostgresSQL
PG Admin
Docker
OpenAPI / Swagger
Heroku
Intellij
Git Bash
Visual studio Code
React
Javascript


⚠ Dependencies

We recommend to not change the versions in the dependencies. Otherwise, the application might not work.


⚡ Installing & Running

🦾 Install
Follow the steps to clone the application.

Clone the repo:


Backend: https://gitlab.com/elon.mansson/legalt.git

Add database settings in Application.properties:
spring.datasource.url=jdbc:postgresql://localhost:5432/(Databasename)
spring.datasource.username=(Username in pgadmin)
spring.datasource.password=(Password in pgadmin)

Frontend: frontend:https://gitlab.com/elon.mansson/keycloak

In the file run npm install to get all the files for the frontend

💻 Executing program
How to run the application:

Build Project - SpringApiApplication.
Run


Front end - React
npm start

😎 Authors
Contributors and contact information
Elon Månsson
@elon.mansson
Simon Roshäll
@simonroshall
Robin Axelstorm
@Robinax

🦝 Version History

0.1

Initial Release




🌌 License
This project is fully free for use.