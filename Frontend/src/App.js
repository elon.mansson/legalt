import "./App.css";
import Footer from "./components/footer/Fotter";
import Nav from "./components/nav/Nav";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import KeycloakRoute from "./routes/KeycloakRoute";
import ProfilePage from "./components/profile/profilePage";
import ProjectList from "./components/projectList/projectList";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Nav></Nav>
        <Routes>
          <Route path="/" element={<ProjectList />}></Route>
          <Route
            path="/profile"
            element={
              <KeycloakRoute>
                <ProfilePage />
              </KeycloakRoute>
            }
          >
            ASD
          </Route>
        </Routes>

        <Footer></Footer>
      </div>
    </BrowserRouter>
  );
}

export default App;
