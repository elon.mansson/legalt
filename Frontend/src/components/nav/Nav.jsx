import './Nav.css';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Nav = () => {

  

    return (
        <div className="navigation-container">
            
            <div className='search-container'> 
                <div className='icon-container'>
                    ICON
                </div>
                <input type="text" placeholder='Search Reddit'/>
            </div>
            <div className='profile-container'>
                <button onClick={() => axios.get('http://localhost:8080/test/1')
                    .then(res => JSON.parse(res))
                    .then(data => console.log(data.name))}>Log in</button>
                <Link to="/profile"><button>Sign up</button></Link>
                dropdown
            </div>
        </div>
    )
}

export default Nav;