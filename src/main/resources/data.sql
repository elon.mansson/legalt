INSERT INTO logged_in_user (logged_in_user_uid,logged_in_user_name,logged_in_user_show) VALUES ('1','Robin', false);
INSERT INTO logged_in_user (logged_in_user_uid,logged_in_user_name,logged_in_user_show) VALUES ('2','Simon', false);
INSERT INTO logged_in_user (logged_in_user_uid,logged_in_user_name,logged_in_user_show) VALUES ('3','Elon', false);

INSERT INTO accomplishment (accomplishment_url, logged_in_user_uid) VALUES ('http://localhost:8080/api/v1/projects', '1');
INSERT INTO accomplishment (accomplishment_url, logged_in_user_uid) VALUES ('http://localhost:8080/api/v1/users', '2');
INSERT INTO accomplishment (accomplishment_url, logged_in_user_uid) VALUES ('http://localhost:8080/api/v1/skills', '3');

INSERT INTO project (project_title,project_desc,project_tag, project_git_repo, logged_in_user_uid) VALUES ('Java-project-RPG', 'This is an rpg game that will be using simple ui and some advanced object oriented coding.', 'GAME_DEVELOPMENT', 'https://gitlab.com/elon.mansson/legalt','1');
INSERT INTO project (project_title,project_desc,project_tag, project_git_repo) VALUES ('C#-project-Shooter', 'this is a shooter game made in c# and it uses the help of advanced ai to make it more difficult for the player.', 'GAME_DEVELOPMENT', 'https://gitlab.com/elon.mansson/legalt');
INSERT INTO project (project_title,project_desc,project_tag, project_git_repo) VALUES ('React-project-Snake', 'this is a game made in react and it works like snake. You are a worm and try to eat as many dots as possible.', 'GAME_DEVELOPMENT', 'https://gitlab.com/elon.mansson/legalt');
INSERT INTO project (project_title,project_desc,project_tag) VALUES ('Music-project', 'Making music with my guitar. From rock to hiphop to gaming music. Everything here is ok to use', 'MUSIC');
INSERT INTO project (project_title,project_desc,project_tag) VALUES ('Ashes of Creation', 'this is a brand new MMO in progress. We are hard at work to deliver something that will be jaw dropping', 'GAME_DEVELOPMENT');
INSERT INTO project (project_title,project_desc,project_tag) VALUES ('website-project-teaching', 'Teaching Angular to everyone!! And remind people to take a break, if there is time for it.', 'WEB_DEVELOPMENT');
INSERT INTO project (project_title,project_desc,project_tag) VALUES ('Movie-project-Streaming', 'this is a new streaming site that will take the world by storm.', 'MOVIE');
INSERT INTO project (project_title,project_desc,project_tag) VALUES ('Python-project-Platformjumper', 'this is a platform game made in python(My first game). More info will come as it progresses', 'GAME_DEVELOPMENT');
INSERT INTO project (project_title,project_desc,project_tag) VALUES ('Angular-project-game', 'this is a game made in angular. More information will come when im testing it.', 'GAME_DEVELOPMENT');
INSERT INTO project (project_title,project_desc,project_tag) VALUES ('Sql-project', 'this is a sql project to test all the syntax in sql.', 'GAME_DEVELOPMENT');
INSERT INTO project (project_title,project_desc,project_tag) VALUES ('website-project-teaching', 'Teaching react to everyone! Loves tea.', 'WEB_DEVELOPMENT');
INSERT INTO project (project_title,project_desc,project_tag) VALUES ('website-project-teaching', 'Teaching Java, springboot. Likes to overcomplicate solutions to katas!', 'WEB_DEVELOPMENT');

INSERT INTO photo (photo_url, project_id) VALUES ('https://images.unsplash.com/photo-1611996575749-79a3a250f948?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8Z2FtZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=600&q=60', 1);
INSERT INTO photo (photo_url, project_id) VALUES ('https://images.unsplash.com/photo-1553481187-be93c21490a9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8Z2FtZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=600&q=60', 2);
INSERT INTO photo (photo_url, project_id) VALUES ('https://images.unsplash.com/photo-1633356122102-3fe601e05bd2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cmVhY3R8ZW58MHx8MHx8&auto=format&fit=crop&w=600&q=60', 3);
INSERT INTO photo (photo_url, project_id) VALUES ('https://images.unsplash.com/photo-1510915361894-db8b60106cb1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80', 4);
INSERT INTO photo (photo_url, project_id) VALUES ('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXz3jZTleLgQmkPZvpxY5J8QbbL5c81PMVc4718SrsnA&s', 5);
INSERT INTO photo (photo_url, project_id) VALUES ('https://static.noroff.no/cms/ansatte/brg/dean-von-schoultz.jpg', 6);
INSERT INTO photo (photo_url, project_id) VALUES ('https://images.unsplash.com/photo-1615986201152-7686a4867f30?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8c3RyZWFtaW5nJTIwdHZ8ZW58MHx8MHx8&auto=format&fit=crop&w=600&q=60', 7);
INSERT INTO photo (photo_url, project_id) VALUES ('https://preview.redd.it/jogf31rlmux41.gif?width=640&crop=smart&format=png8&s=33ca84e1e126c4783493844016dd392f336a14c2', 8);
INSERT INTO photo (photo_url, project_id) VALUES ('https://images.unsplash.com/10/wii.jpg?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8OXx8Z2FtZXxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=600&q=60', 9);
INSERT INTO photo (photo_url, project_id) VALUES ('https://media.istockphoto.com/photos/wooden-mannequin-demonstrating-this-word-picture-id185085453?b=1&k=20&m=185085453&s=170667a&w=0&h=Xu2LVTiykimHnzferN48bi8wSqpGE05wKM8r69WLN2Q=', 10);
INSERT INTO photo (photo_url, project_id) VALUES ('https://static.noroff.no/cms/ansatte/acc/jean-clive-bailey.jpg', 11);
INSERT INTO photo (photo_url, project_id) VALUES ('https://static.noroff.no/cms/ansatte/acc/sean-skinner.jpg', 12);

INSERT INTO skill (skill_title) VALUES ('Programming');
INSERT INTO skill (skill_title) VALUES ('Movies');
INSERT INTO skill (skill_title) VALUES ('Sports');

INSERT INTO message (message_comment, logged_in_user_uid, project_id) VALUES ('Hello!', '1', 2);
INSERT INTO message (message_comment, logged_in_user_uid, project_id) VALUES ('How are you doing?', '2', 2);
INSERT INTO message (message_comment, logged_in_user_uid, project_id) VALUES ('Cool project!', '2', 2);

INSERT INTO logged_in_user_project (project_id,logged_in_user_uid) VALUES (6,'2');
INSERT INTO logged_in_user_project (project_id, logged_in_user_uid) VALUES (3,'2');
INSERT INTO logged_in_user_project (project_id, logged_in_user_uid) VALUES (2,'1');
INSERT INTO logged_in_user_project (project_id, logged_in_user_uid) VALUES (1,'2');
INSERT INTO logged_in_user_project (project_id, logged_in_user_uid) VALUES (2,'2');
INSERT INTO logged_in_user_project (project_id, logged_in_user_uid) VALUES (3,'1');
INSERT INTO logged_in_user_project (project_id, logged_in_user_uid) VALUES (3,'3');

INSERT INTO logged_in_user_skill (logged_in_user_uid, skill_id) VALUES ('1',1);
INSERT INTO logged_in_user_skill (logged_in_user_uid, skill_id) VALUES ('1',2);
INSERT INTO logged_in_user_skill (logged_in_user_uid, skill_id) VALUES ('2',2);
INSERT INTO logged_in_user_skill (logged_in_user_uid, skill_id) VALUES ('3',1);
INSERT INTO logged_in_user_skill (logged_in_user_uid, skill_id) VALUES ('3',3);

