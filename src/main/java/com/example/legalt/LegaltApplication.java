package com.example.legalt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LegaltApplication {

    public static void main(String[] args) {
        SpringApplication.run(LegaltApplication.class, args);
    }

}
