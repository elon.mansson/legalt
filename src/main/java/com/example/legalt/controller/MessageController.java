package com.example.legalt.controller;

import com.example.legalt.mappers.message.MessageMapper;
import com.example.legalt.mappers.message.MessageWithUserNameMapper;
import com.example.legalt.model.Message;
import com.example.legalt.model.dtos.Messages.Message.CreateMessageDTO;
import com.example.legalt.model.dtos.Messages.Message.MessageDTO;
import com.example.legalt.model.dtos.Messages.Message.MessageWithUserNameDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Message.MessageService;
import com.example.legalt.service.Project.ProjectService;
import com.example.legalt.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/message")
@CrossOrigin(origins = "https://legalt-frontend.herokuapp.com/")
public class MessageController {
    private final MessageService messageService;
    private final ProjectService projectService;
    private final MessageMapper messageMapper;
    private final MessageWithUserNameMapper messageWithUserNameMapper;


    private final LoggedInUserService loggedInUserService;

    public MessageController(MessageService messageService, MessageMapper messageMapper, MessageWithUserNameMapper messageWithUserNameMapper, ProjectService projectService, LoggedInUserService loggedInUserService) {
        this.messageService = messageService;
        this.messageMapper = messageMapper;
        this.messageWithUserNameMapper = messageWithUserNameMapper;
        this.projectService = projectService;
        this.loggedInUserService = loggedInUserService;
    }
    //Gets all messages.
    @Operation(summary = "get all messages")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MessageDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the messages",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity findAll() {
        Collection<MessageDTO> messages = messageMapper.messageToMessageDTO(messageService.findAll());
        return ResponseEntity.ok(messages);
    }
    //Get message by id
    @Operation(summary = "get message by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MessageDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the message with the id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id){
        MessageDTO messageDTO = messageMapper.messageToMessageDTO(
                messageService.findById(id)
        );
        return ResponseEntity.ok(messageDTO);
    }
    //Adds a new message
    @Operation(summary = "add new message")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateMessageDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't add the message",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping("/{id}")
    public ResponseEntity addMessage(@RequestBody CreateMessageDTO newMessage, @PathVariable int id) {
            //Get current project and user to set the project with the right ids.
            var currentProject = projectService.findById(id);
            var projectMessages = currentProject.getMessages();
            var currentUser = loggedInUserService.findById(newMessage.getLoggedInUser());

            Message oneMessage = messageMapper.messageDtoToMessage(newMessage);
            oneMessage.setProject(currentProject);
            oneMessage.setLoggedInUser(currentUser);

            Message message = messageService.add(oneMessage);
            URI uri = URI.create("messages/" + message.getId());
            projectMessages.add(oneMessage);

            return ResponseEntity.created(uri).build();
        }

    //Updates a message by id
    @Operation(summary = "update a message")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MessageDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't update the message",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Message not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity updateMessage(@RequestBody MessageDTO messageDTO, @PathVariable int id){
        if (id != messageDTO.getId()) {
            return ResponseEntity.badRequest().build();
        }

            messageService.update(
                messageMapper.messageDtoToMessage(messageDTO)
        );
        return ResponseEntity.noContent().build();
    }
    //Delete a message by id
    @Operation(summary = "delete a message")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't delete the message",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("{id}")
    public ResponseEntity deleteMessage(@PathVariable int id) {
        messageService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
