package com.example.legalt.controller;

import com.example.legalt.mappers.history.HistoryMapper;
import com.example.legalt.mappers.loggedInUser.LoggedInUserMapper;
import com.example.legalt.mappers.message.MessageMapper;
import com.example.legalt.mappers.photo.PhotoMapper;
import com.example.legalt.mappers.project.*;
import com.example.legalt.model.History;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.LoggedInUser.LoggedInUserDTO;
import com.example.legalt.model.dtos.Project.*;
import com.example.legalt.service.History.HistoryService;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Photo.PhotoService;
import com.example.legalt.service.Project.ProjectService;
import com.example.legalt.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.*;

@RestController
@RequestMapping(path = "api/v1/projects")
@CrossOrigin(origins = "https://legalt-frontend.herokuapp.com/")
public class ProjectController {
    private final ProjectService projectService;
    private final ProjectMapper projectMapper;
    private final MessageMapper messageMapper;
    private final LoggedInUserService loggedInUserService;
    private final LoggedInUserMapper loggedInUserMapper;
    private final ProjectMapperLoggedIn projectMapperLoggedIn;
    private final ProjectMapperNotLoggedIn projectMapperNotLoggedIn;
    private final ProjectAdminMapper projectAdminMapper;
    private final CreateProjectMapper createProjectMapper;
    private final UpdateProjectAdminMapper updateProjectAdminMapper;
    private final ProjectChangeUserMapper projectChangeUserMapper;
    private final ProjectChangeRequestMapper projectChangeRequestMapper;
    private final DeleteProjectMapper deleteProjectMapper;
    private final ProjectAddToHistoryMapper projectAddToHistoryMapper;
    private final PhotoMapper photoMapper;
    private final PhotoService photoService;
    private final HistoryMapper historyMapper;
    private final HistoryService historyService;


    public ProjectController(ProjectService projectService, ProjectMapper projectMapper, MessageMapper messageMapper, LoggedInUserMapper loggedInUserMapper, LoggedInUserService loggedInUserService, ProjectMapperLoggedIn projectMapperLoggedIn, ProjectMapperNotLoggedIn projectMapperNotLoggedIn, ProjectAdminMapper projectAdminMapper, CreateProjectMapper createProjectMapper, UpdateProjectAdminMapper updateProjectAdminMapper, ProjectChangeUserMapper projectChangeUserMapper, ProjectChangeRequestMapper projectChangeRequestMapper, DeleteProjectMapper deleteProjectMapper, ProjectAddToHistoryMapper projectAddToHistoryMapper, PhotoMapper photoMapper, PhotoService photoService, HistoryMapper historyMapper, HistoryService historyService) {
        this.projectService = projectService;
        this.projectMapper = projectMapper;
        this.messageMapper = messageMapper;
        this.loggedInUserService = loggedInUserService;
        this.loggedInUserMapper = loggedInUserMapper;
        this.projectMapperLoggedIn = projectMapperLoggedIn;
        this.projectMapperNotLoggedIn = projectMapperNotLoggedIn;
        this.projectAdminMapper = projectAdminMapper;
        this.createProjectMapper = createProjectMapper;
        this.updateProjectAdminMapper = updateProjectAdminMapper;
        this.projectChangeUserMapper = projectChangeUserMapper;
        this.projectChangeRequestMapper = projectChangeRequestMapper;
        this.deleteProjectMapper = deleteProjectMapper;
        this.projectAddToHistoryMapper = projectAddToHistoryMapper;
        this.photoMapper = photoMapper;
        this.photoService = photoService;
        this.historyMapper = historyMapper;
        this.historyService = historyService;
    }

    @Operation(summary = "get all projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity findAll() {
        Collection<ProjectNotLoggedInDTO> projects = projectMapperNotLoggedIn.projectToProjectDto(projectService.findAll());
        return ResponseEntity.ok(projects);
    }

    @Operation(summary = "get projects based on history")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectNotLoggedInDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{uid}")
    public ResponseEntity findAllById(@PathVariable String uid) {
        //algorithm to sort list for users interests
        var tagCounter = new HashMap<String,Integer>();
        var allProjects = projectService.findAll().stream().toList();
        var history = loggedInUserService.findById(uid).getHistory().stream().toList();
        var myHistory = historyMapper.HistoryToHistoryDto(history).stream().toList();

        for (int i = 0; i < allProjects.size(); i++) {
            var tag = allProjects.get(i).getTag();
            tagCounter.put(tag,0);
        }

        for (int i = 0; i < myHistory.size(); i++) {
            int counter = 0;
            counter = tagCounter.get(myHistory.get(i).getProject().getTag()) + 1;
            tagCounter.replace(myHistory.get(i).getProject().getTag(),counter);
        }

        LinkedHashMap<String, Integer> sortedMap = new LinkedHashMap<>();
        ArrayList<Integer> list = new ArrayList<>();

        for (Map.Entry<String, Integer> entry : tagCounter.entrySet()) {
            list.add(entry.getValue());
        }
        Collections.sort(list);
        for (int num : list) {
            for (Map.Entry<String, Integer> entry : tagCounter.entrySet()) {
                if (entry.getValue().equals(num)) {
                    sortedMap.put(entry.getKey(), num);
                }
            }
        }

        var tagList = new ArrayList<>(sortedMap.keySet().stream().toList());
        Collections.reverse(tagList);

        var newList = new ArrayList<ProjectNotLoggedInDTO>();

        for (int i = 0; i < tagList.size(); i++) {
            for (int j = 0; j < allProjects.size(); j++) {
                if(tagList.get(i).equals(allProjects.get(j).getTag())) {
                    newList.add(projectMapperNotLoggedIn.projectToProjectDto(allProjects.get(j)));
                }
            }
        }
        return ResponseEntity.ok(newList);
    }


    @Operation(summary = "add new project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateProjectDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't add the project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping
    public ResponseEntity addProject(@RequestBody CreateProjectDTO newProject) {
        // mapping the dto to a project and adding it.
        Project oneProject = createProjectMapper.projectDtoToProject(newProject);
        Project project = projectService.add(oneProject);
        // adding the project to the user who is admin.
        var user = loggedInUserService.findById(oneProject.getAdmin().getUid());
        var projects = (user.getProjects());
        projects.add(project);
        loggedInUserService.update(user);
        URI uri = URI.create("projects/" + project.getId());
        return ResponseEntity.created(uri).build();
    }
    @PutMapping("/{id}")
    public ResponseEntity updateProject(@RequestBody UpdateProjectPutDTO updateProjectAdminDTO, @PathVariable int id) {
    var currentProject = projectService.findById(id);
    var updateTitle = updateProjectAdminDTO.getTitle();
    var updateDescription = updateProjectAdminDTO.getDesc();
    var updateGitRepo = updateProjectAdminDTO.getGitRepo();
    var updateTag = updateProjectAdminDTO.getTag();

    if (updateTitle != null) currentProject.setTitle(updateTitle);
    if (updateDescription != null) currentProject.setDesc(updateDescription);
    if (updateGitRepo != null) currentProject.setGitRepo(updateGitRepo);
    if (updateTag != null) currentProject.setTag(updateTag);

    projectService.update(currentProject);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "admin update a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UpdateProjectAdminDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't update the project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Project not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}/admin")
    public ResponseEntity updateProjectAdmin(@RequestBody UpdateProjectAdminDTO projectDTO, @PathVariable int id) {
        if (id != projectDTO.getId()) {
            return ResponseEntity.badRequest().build();
        }
        var currenProject = projectService.findById(id);
        var updatedTitle = updateProjectAdminMapper.projectDtoToProject(projectDTO).getTitle();
        var updatedDesc = updateProjectAdminMapper.projectDtoToProject(projectDTO).getDesc();
        var updatedGitRepo = updateProjectAdminMapper.projectDtoToProject(projectDTO).getGitRepo();
        if (updatedTitle != null) {
            currenProject.setTitle(updatedTitle);
        }
        if (updatedDesc != null) {
            currenProject.setDesc(updatedDesc);
        }
        if (updatedGitRepo != null) {
            currenProject.setGitRepo(updatedGitRepo);
        }
        projectService.update(currenProject);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "add user to project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectChangeUserDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't add user to the project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Project not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}/adduser")
    public ResponseEntity addUserToProject(@RequestBody ProjectChangeUserDTO projectDTO, @PathVariable int id) {
        var project = projectService.findById(id);
        var projectUsers = projectChangeUserMapper.projectToProjectDto(project).getLoggedInUsers().stream().toList();
        var users = projectDTO.getLoggedInUsers().toArray();
        var uid = users[0].toString();

        for (int i = 1; i < projectUsers.size(); i++) {
            if (uid.equals(projectUsers.get(i))) {
                return ResponseEntity.badRequest().build();
            }
        }
        // adding the user to the user list in the project.
        var oneUser = loggedInUserService.findById(uid);
        var oneProject = projectService.findById(id);
        oneProject.getLoggedInUsers().add(oneUser);
        projectService.update(oneProject);
        ResponseEntity.noContent().build();

        //adding the project to the users project list.
        oneUser.getProjects().add(oneProject);
        loggedInUserService.update(oneUser);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "remove user from project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectChangeUserDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't remove user from the project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Project not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}/removeuser")
    public ResponseEntity removeUserFromProject(@RequestBody ProjectChangeUserDTO projectDTO, @PathVariable int id) {
        var project = projectService.findById(id);
        var projectUsers = projectChangeUserMapper.projectToProjectDto(project).getLoggedInUsers().toArray();
        var users = projectDTO.getLoggedInUsers().toArray();
        var uid = users[0].toString();

        for (int i = 0; i < projectUsers.length; i++) {
            if (uid.equals(projectUsers[i])) {

                // removing user to projects user list.
                var oneUser = loggedInUserService.findById(uid);
                var oneProject = projectService.findById(id);
                oneProject.getLoggedInUsers().remove(oneUser);
                projectService.update(oneProject);
                ResponseEntity.noContent().build();

                // removing project from users project list.
                oneUser.getProjects().remove(oneProject);
                loggedInUserService.update(oneUser);
                return ResponseEntity.noContent().build();
            }
        }
        return ResponseEntity.badRequest().build();
    }

    @Operation(summary = "delete a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't delete the project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("/{id}")
    public ResponseEntity deleteProject(@PathVariable int id) {
        Project project = projectService.findById(id);
        var admin = project.getAdmin();
        var dto = deleteProjectMapper.projectToProjectDto(project);
        var usersInProject = dto.getLoggedInUsers().toArray();
        var requestUsers = dto.getRequestUsers().toArray();
        var messages = dto.getMessages().toArray();
        var photos = dto.getPhotos().toArray();
        var history = dto.getHistory().toArray();
        project.setHistory(null);

        if (project.getAdmin() != null) {
            admin.getAdminProjects().remove(project);
        }
        for (int i = 0; i < usersInProject.length; i++) {
            var user = loggedInUserService.findById(usersInProject[i].toString());
            user.getProjects().remove(project);
            loggedInUserService.update(user);
        }

        for (int i = 0; i < messages.length; i++) {
            var user = loggedInUserService.findById(messages[i].toString());
            user.getMessages().remove(project);
            loggedInUserService.update(user);
        }
        for (int i = 0; i < requestUsers.length; i++) {
            var user = loggedInUserService.findById(requestUsers[i].toString());
            user.getRequests().remove(project);
            loggedInUserService.update(user);
        }
        for (int i = 0; i < photos.length; i++) {
            var photo = photoService.findById(Integer.parseInt(photos[i].toString()));
            photo.setProject(null);
            photoService.update(photo);
        }
        for (int i = 0; i < history.length; i++) {
            var oneHistory = historyService.findById(Integer.parseInt(history[i].toString()));
            oneHistory.setProject(null);
            oneHistory.setLoggedInUser(null);
            historyService.deleteById(oneHistory.getId());
        }

        // setting all values in project that's referencing other tables to null.
        project.setAdmin(null);
        project.setLoggedInUsers(null);
        project.setRequestUsers(null);
        project.setMessages(null);
        project.setPhotos(null);


        projectService.update(project);
        projectService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "get users from project id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = LoggedInUserDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the users with the project id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}/users")
    public ResponseEntity findUsersWithId(@PathVariable int id) {
        Collection<LoggedInUserDTO> usersById = loggedInUserMapper.loggedInUserToLoggedInUserDTO(
                projectService.findById(id).getLoggedInUsers()
        );
        return ResponseEntity.ok(usersById);
    }

    @Operation(summary = "get projects when user is logged in")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectLoggedInDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}/loggedin")
    public ResponseEntity loggedInFindById(@PathVariable int id) {
        ProjectLoggedInDTO projectDTO = projectMapperLoggedIn.projectToProjectDto(
                projectService.findById(id)

        );
        return ResponseEntity.ok(projectDTO);
    }

    @Operation(summary = "get projects when user is admin")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectAdminDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}/admin")
    public ResponseEntity adminFindById(@PathVariable int id) {
        ProjectAdminDTO projectDTO = projectAdminMapper.projectToProjectDto(
                projectService.findById(id)
        );
        return ResponseEntity.ok(projectDTO);
    }

    @Operation(summary = "add request from user to join project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectChangeUserDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't add request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Project not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}/addrequest")
    public ResponseEntity requestProject(@RequestBody ProjectChangeUserDTO projectDTO, @PathVariable int id) {
        var project = projectService.findById(id);
        var requestUsers = projectChangeRequestMapper.projectToProjectDto(project).getRequestUsers().toArray();
        var users = projectDTO.getLoggedInUsers().toArray();
        var uid = users[0].toString();

        for (int i = 0; i < requestUsers.length; i++) {
            // if the user already has a pending request to the project, a badRequest is returned.
            if (uid.equals(requestUsers[i].toString())) {
                return ResponseEntity.badRequest().build();
            }
        }
        // the user is added to the requests list in the project
        var oneUser = loggedInUserService.findById(uid);
        var oneProject = projectService.findById(id);
        oneProject.getRequestUsers().add(oneUser);
        projectService.update(oneProject);
        ResponseEntity.noContent().build();

        // the project is added to the users projectRequests list.
        oneUser.getRequests().add(oneProject);
        loggedInUserService.update(oneUser);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "handle request from user to join project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectChangeRequestDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't remove/add user from the project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Project not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}/handlerequest")
    public ResponseEntity handleRequestFromProject(@RequestBody ProjectChangeRequestDTO projectDTO, @PathVariable int id) {
        var project = projectService.findById(id);
        var requestUsers = project.getRequestUsers().stream().toList();
        var users = projectDTO.getRequestUsers().toArray();
        var uid = users[0].toString();

        for (int i = 0; i < requestUsers.size(); i++) {
            var currUser = requestUsers.get(i).getUid();
            if (uid.equals(currUser)) {
                // if the admin accepts the request, a dto is created with the users uid and the method addUserToProject is invoked.
                // both the user dto and the project id is sent as arguments.
                if (projectDTO.isAccept()) {
                    ProjectChangeUserDTO dto = new ProjectChangeUserDTO();
                    dto.setLoggedInUsers(Set.of(uid));
                    addUserToProject(dto, id);
                }
                //removing the user from the requestUsers list in the specific project.
                var oneUser = loggedInUserService.findById(uid);
                var oneProject = projectService.findById(id);
                oneProject.getRequestUsers().remove(oneUser);
                projectService.update(oneProject);
                ResponseEntity.noContent().build();

                //removing the project from the requests in the users list
                oneUser.getRequests().remove(oneProject);
                loggedInUserService.update(oneUser);

                return ResponseEntity.noContent().build();
            }
        }
        return ResponseEntity.badRequest().build();
    }

    @Operation(summary = "add project to the users history")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectAddToHistoryDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't add project to the users history",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Project not found with supplied ID",
                    content = @Content)
    })
    @PostMapping("/{id}/addhistory")
    public ResponseEntity addToHistory(@RequestBody ProjectAddToHistoryDTO projectAddToHistoryDTO, @PathVariable int id) {
        var uid = projectAddToHistoryDTO.getUid();
        var user = loggedInUserService.findById(uid);
        var project = projectService.findById(id);
        var newHistory = new History(user,project);
        historyService.add(newHistory);
        historyService.update(newHistory);
        return ResponseEntity.noContent().build();
    }
}