package com.example.legalt.controller;

import com.example.legalt.mappers.photo.CreatePhotoMapper;
import com.example.legalt.model.Photo;
import com.example.legalt.model.dtos.Messages.Message.CreateMessageDTO;
import com.example.legalt.model.dtos.Photo.CreatePhotoDTO;
import com.example.legalt.service.Photo.PhotoService;
import com.example.legalt.service.Project.ProjectService;
import com.example.legalt.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Set;

@RestController
@RequestMapping(path = "api/v1/photos")
@CrossOrigin(origins = "https://legalt-frontend.herokuapp.com/")
public class PhotoController {

    private final PhotoService photoService;
    private final CreatePhotoMapper createPhotoMapper;
    private final ProjectService projectService;

    public PhotoController(PhotoService photoService, CreatePhotoMapper createPhotoMapper, ProjectService projectService) {
        this.photoService = photoService;
        this.createPhotoMapper = createPhotoMapper;
        this.projectService = projectService;
    }
    //Adds photo
    @Operation(summary = "add new photo")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateMessageDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't add the photo",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PutMapping
    public ResponseEntity addPhoto(@RequestBody CreatePhotoDTO newPhoto) {
        var onePhoto = createPhotoMapper.photoDtoToPhoto(newPhoto);
        var projectId = newPhoto.getProject();

        var oldphotos = projectService.findById(projectId).getPhotos();
        for (Photo oldphoto : oldphotos) {
            oldphotos.remove(oldphoto);
            photoService.deleteById(oldphoto.getId());
        }
        photoService.add(onePhoto);

        projectService.update(projectService.findById(projectId));
        URI uri = URI.create("photos/" + onePhoto.getId());
        return ResponseEntity.created(uri).build();
    }
}