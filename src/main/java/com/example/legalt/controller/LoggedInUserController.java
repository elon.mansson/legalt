package com.example.legalt.controller;

import com.example.legalt.mappers.accomplishment.AccomplishmentMapper;
import com.example.legalt.mappers.history.HistoryMapper;
import com.example.legalt.mappers.loggedInUser.LoggedInUserMapper;
import com.example.legalt.mappers.loggedInUser.LoggedInUserPPMapper;
import com.example.legalt.mappers.loggedInUser.UpdateUserMapper;
import com.example.legalt.mappers.project.ProjectMapper;
import com.example.legalt.mappers.project.ProjectMapperLoggedIn;
import com.example.legalt.mappers.skill.SkillMapper;
import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.dtos.Accomplishment.AccomplishmentDTO;
import com.example.legalt.model.dtos.LoggedInUser.*;
import com.example.legalt.model.dtos.Project.HistoryProjectDTO;
import com.example.legalt.model.dtos.Project.ProjectDTO;
import com.example.legalt.model.dtos.Skill.SkillDTO;
import com.example.legalt.model.dtos.Skill.RemoveSkillDto;
import com.example.legalt.service.History.HistoryService;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Skill.SkillService;
import com.example.legalt.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/users")
@CrossOrigin(origins = "https://legalt-frontend.herokuapp.com/")
public class LoggedInUserController {

    private final LoggedInUserService loggedInUserService;
    private final LoggedInUserMapper loggedInUserMapper;
    private final SkillService skillService;
    private final ProjectMapper projectMapper;
    private final SkillMapper skillMapper;
    private final UpdateUserMapper updateUserMapper;
    private final AccomplishmentMapper accomplishmentMapper;
    private final LoggedInUserPPMapper loggedInUserPPMapper;
    private final ProjectMapperLoggedIn projectMapperLoggedIn;
    private final HistoryMapper historyMapper;
    private final HistoryService historyService;

    public LoggedInUserController(LoggedInUserService loggedInUserService, LoggedInUserMapper loggedInUserMapper, SkillService skillService, ProjectMapper projectMapper, SkillMapper skillMapper, UpdateUserMapper updateUserMapper, AccomplishmentMapper accomplishmentMapper, LoggedInUserPPMapper loggedInUserPPMapper, ProjectMapperLoggedIn projectMapperLoggedIn, HistoryMapper historyMapper, HistoryService historyService) {
        this.loggedInUserService = loggedInUserService;
        this.loggedInUserMapper = loggedInUserMapper;
        this.skillService = skillService;
        this.projectMapper = projectMapper;
        this.skillMapper = skillMapper;
        this.updateUserMapper = updateUserMapper;
        this.accomplishmentMapper = accomplishmentMapper;
        this.loggedInUserPPMapper = loggedInUserPPMapper;
        this.projectMapperLoggedIn = projectMapperLoggedIn;
        this.historyMapper = historyMapper;
        this.historyService = historyService;
    }

//Swagger configs for get all users and a get mapping for it.
    @Operation(summary = "get all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = LoggedInUserDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the users",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity findAll() {
        Collection<LoggedInUserDTO> users = loggedInUserMapper.loggedInUserToLoggedInUserDTO(loggedInUserService.findAll());
        return ResponseEntity.ok(users);
    }
    //Get user by id and swagger config.
    @Operation(summary = "get user by uid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = LoggedInUserDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the user with the uid",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{uid}")
    public ResponseEntity findById(@PathVariable String uid){
        LoggedInUserDTO loggedInUserDTO = loggedInUserMapper.loggedInUserToLoggedInUserDTO(
                loggedInUserService.findById(uid)
        );
        return ResponseEntity.ok(loggedInUserDTO);
    }
    //Post mapping to create a new user and gives the user the keycloak sub key as uid.
    @Operation(summary = "add new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateLoggedInUserDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't add the user",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping("register")
    public ResponseEntity addNewUserFromJwt(@AuthenticationPrincipal Jwt jwt) throws Exception {
        LoggedInUser user = loggedInUserService.add(jwt.getClaimAsString("sub"),jwt.getClaimAsString("given_name"));
        URI uri = URI.create("api/v1/users/" + user.getUid());
        return ResponseEntity.created(uri).build();
    }
    //Updates user with some swagger configs
    @Operation(summary = "update a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = LoggedInUserDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't update the user",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "User not found with supplied UID",
                    content = @Content)
    })
    @PutMapping("/{uid}")
    public ResponseEntity updateUser(@RequestBody UpdateLoggedInUserDTO updateLoggedInUserDTO, @PathVariable String uid){
        if (!uid.equals(uid)) {
            return ResponseEntity.badRequest().build();
        }
        var currentUser = loggedInUserService.findById(uid);
        var updatedDescription =  updateUserMapper.UpdateloggedInUserDtoToLoggedInUser(updateLoggedInUserDTO).getDescription();
        var updateShow =  updateUserMapper.UpdateloggedInUserDtoToLoggedInUser(updateLoggedInUserDTO).getShow();
        if (updatedDescription != null) currentUser.setDescription(updatedDescription);
        if (updateShow != currentUser.getShow()) currentUser.setShow(updateShow);

        loggedInUserService.update((currentUser));
        return ResponseEntity.noContent().build();
    }
    //Delete users
    @Operation(summary = "delete a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't delete the user",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("{uid}")
    public ResponseEntity deleteUser(@PathVariable String uid) {
        loggedInUserService.deleteById(uid);
        return ResponseEntity.noContent().build();
    }
    //Get user projects by uid
    @Operation(summary = "get projects from users uid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = LoggedInUserDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the projects with the user uid",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{uid}/projects")
    public ResponseEntity findProjectsWithId(@PathVariable String uid) {
        Collection<ProjectDTO> projectsById = projectMapper.projectToProjectDto(
                loggedInUserService.findById(uid).getProjects()
        );
        return ResponseEntity.ok(projectsById);
    }
    //Get skill from a user by uid.
    @Operation(summary = "get skills from users uid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SkillDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the skills with the user uid",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{uid}/skills")
    public ResponseEntity findSkillsWithId(@PathVariable String uid) {
        Collection<SkillDTO> skillsById = skillMapper.skillToSkillDTO(
                loggedInUserService.findById(uid).getSkills()
        );
        return ResponseEntity.ok(skillsById);
    }
    //Get project in wich the user is the admin by uid
    @Operation(summary = "get admin projects from users uid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the admin projects with the user uid",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}/adminprojects")
    public ResponseEntity findAdminProjectsWithId(@PathVariable String uid) {
        Collection<ProjectDTO> projectsById = projectMapper.projectToProjectDto(
                loggedInUserService.findById(uid).getAdminProjects()
        );
        return ResponseEntity.ok(projectsById);
    }
    //Gets the history for a user by uid.
    @Operation(summary = "get history projects from users uid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProjectDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the history projects with the user uid",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{uid}/historyprojects")
    public ResponseEntity findHistoryProjectsWithId(@PathVariable String uid) {
        Collection<HistoryProjectDTO> projectsById = historyMapper.HistoryToHistoryDto(
                loggedInUserService.findById(uid).getHistory()
        );
        return ResponseEntity.ok(projectsById);
    }
    //Get user accomplishments by uid.
    @Operation(summary = "get accomplishments from users uid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = AccomplishmentDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the accomplishments with the user uid",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{uid}/accomplishments")
    public ResponseEntity findAccomplishmentsWithId(@PathVariable String uid) {
        Collection<AccomplishmentDTO> accomplishmentById = accomplishmentMapper.accomplishmentToAccomplishmentDto(
                loggedInUserService.findById(uid).getAccomplishments()
        );
        return ResponseEntity.ok(accomplishmentById);
    }
    //Get user profilepage so all the information about the user is not shown in network and the front end
    @Operation(summary = "get user by id for profile page")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = LoggedInUserPPDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the user with the uid",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{uid}/profilePage")
    public ResponseEntity findByIdPp(@PathVariable String uid){
        LoggedInUserPPDTO loggedInUserppDTO = loggedInUserPPMapper.loggedInUserToLoggedInUserDTO(
                loggedInUserService.findById(uid)
        );
        return ResponseEntity.ok(loggedInUserppDTO);
    }
    //Deletes the skill from an user. put is put here to update all the tables linked to skill
    @Operation(summary = "Change the users skill")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = LoggedInUserPPDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the user with the uid",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "400",
            description = "There is no skill with that name",
            content = {@Content(mediaType = "application/json",
            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PutMapping("/skills/{id}")
    public ResponseEntity deleteSkill(@RequestBody RemoveSkillDto removeSkillDto, @PathVariable int id){
        var currentUser = loggedInUserService.findById(removeSkillDto.getUid());
        var userSkills = currentUser.getSkills().stream().toList();
        var skills= skillService.findById(id);
        var skillid = skills;
        //looping through all the users skill to check if there is a skill that matches the user.
        //Then update the user aswell as the skill tabel to remove it.
        for (int i = 0; i < userSkills.size();i++){
            if (skillid.equals(userSkills.get(i))){
                var oneSkill = skillService.findById(id);
                oneSkill.getLoggedInUsers().remove(currentUser);
                skillService.update(oneSkill);
                ResponseEntity.noContent().build();
                currentUser.getSkills().remove(oneSkill);
                loggedInUserService.update(currentUser);
                return ResponseEntity.ok().build();
            }
        }

        return ResponseEntity.badRequest().build();
    }
}
