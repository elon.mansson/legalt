package com.example.legalt.controller;

import com.example.legalt.mappers.skill.SkillMapper;
import com.example.legalt.model.Skill;
import com.example.legalt.model.dtos.Skill.CreateSkillDTO;
import com.example.legalt.model.dtos.Skill.SkillDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Skill.SkillService;
import com.example.legalt.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/skills")
@CrossOrigin(origins = "https://legalt-frontend.herokuapp.com/")
public class SkillController {
    private final SkillService skillService;
    private final SkillMapper skillMapper;
    private final LoggedInUserService loggedInUserService;

    public SkillController(SkillService skillService, SkillMapper skillMapper, LoggedInUserService loggedInUserService) {
        this.skillService = skillService;
        this.skillMapper = skillMapper;
        this.loggedInUserService = loggedInUserService;
    }
    //Get all skills in the skill table
    @Operation(summary = "get all skills")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SkillDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateSkillDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the skills",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity findAll() {
        Collection<SkillDTO> skills = skillMapper.skillToSkillDTO(skillService.findAll());
        return ResponseEntity.ok(skills);
    }
    //Get skill by id
    @Operation(summary = "get skill by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SkillDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateSkillDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't get the skill with the id",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id){
        SkillDTO skillDTO = skillMapper.skillToSkillDTO(
                skillService.findById(id)
        );
        return ResponseEntity.ok(skillDTO);
    }
    //Adds new skill to the skill table and the user by users uid.
    @Operation(summary = "add new skill")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateSkillDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateSkillDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't add the skill",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping("/{uid}")
    public ResponseEntity addSkill(@RequestBody SkillDTO newSkill,@PathVariable String uid) {
        var newSkills = new Skill();
        var checkNewSkill= newSkill.getTitle().toLowerCase();
        System.out.println(checkNewSkill);
        var currentUserskills= loggedInUserService.findById(uid).getSkills().stream().toList();
        //If there already is a skill with that name then return before creating a new.
        for (Skill currentUserskill : currentUserskills) {
            if (currentUserskill.getTitle().equals(newSkill.getTitle().toLowerCase())) {
                return ResponseEntity.badRequest().build();
            }
        }
        //Checking if the skill exists in the service and if it exists it adds it to the user by uid and if not it creates it in the skill table
        for (int i = 1; i <= skillService.findAll().size();i++ ){
           var getSkills = skillService.findById(i).getTitle().toLowerCase();

           if (checkNewSkill.equals(getSkills)){
               newSkills = skillService.findById(i);
           }
           else if(i == skillService.findAll().size()){
               Skill oneSkill = skillMapper.skillDtoToSkill(newSkill);
               oneSkill.setTitle(oneSkill.getTitle().toLowerCase());
               Skill skill = skillService.add(oneSkill);
               URI uri = URI.create("skills/" + skill.getId());
               newSkills = oneSkill;
               ResponseEntity.created(uri).build();
           }
       }
       var currentUser = loggedInUserService.findById(uid);
       var getUsersSkills= currentUser.getSkills();
       getUsersSkills.add(newSkills);
       currentUser.setSkills(getUsersSkills);
       loggedInUserService.update(currentUser);
       return ResponseEntity.ok().build();
    }
    //Updates skill by id
    @Operation(summary = "update a skill")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SkillDTO.class))
                    }),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateSkillDTO.class))
                    }),
            @ApiResponse(responseCode = "400",
                    description = "Couldn't update the skill",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Skill not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity updateSkill(@RequestBody SkillDTO skillDTO, @PathVariable int id){
        if (id != skillDTO.getId())
            return ResponseEntity.badRequest().build();
        skillService.update(
                skillMapper.skillDtoToSkill(skillDTO)
        );
        return ResponseEntity.noContent().build();
    }

    //Deletes a skill by id
    @Operation(summary = "delete a skill")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Success",
                    content = {@Content(mediaType = "application/json"
                    )}),
            @ApiResponse(responseCode = "401",
                    description = "You need a token.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CreateSkillDTO.class))
                    }),
            @ApiResponse(responseCode = "404",
                    description = "Couldn't delete the skill",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @DeleteMapping("{id}")
    public ResponseEntity deleteSkill(@PathVariable int id) {
       var users = loggedInUserService.findAll().stream().toList();

        for (int i =1;i< users.size();i++){
           var getUserSkill = users.get(i).getSkills();
            if (getUserSkill.equals(skillService.findById(id))){
                getUserSkill.remove(skillService.findById(id));
                users.get(i).setSkills(getUserSkill);
                loggedInUserService.update(users.get(i));

            }
        }

        skillService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}