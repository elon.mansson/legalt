package com.example.legalt.model.dtos.Project;

import lombok.Data;

@Data
public class ProjectAddToHistoryDTO {
    private String uid;
}
