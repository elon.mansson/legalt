package com.example.legalt.model.dtos.Photo;

import lombok.Data;

@Data
public class PhotoDTO {
    private int id;
    private String url;
    private int project;
}
