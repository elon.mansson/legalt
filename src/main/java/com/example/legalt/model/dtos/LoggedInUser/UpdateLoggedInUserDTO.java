package com.example.legalt.model.dtos.LoggedInUser;

import lombok.Data;


@Data
public class UpdateLoggedInUserDTO {

    private boolean show;
    private String description;

}
