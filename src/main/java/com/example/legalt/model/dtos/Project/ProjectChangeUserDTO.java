package com.example.legalt.model.dtos.Project;

import lombok.Data;

import java.util.Set;

@Data
public class ProjectChangeUserDTO {
    private Set<String> loggedInUsers;
}