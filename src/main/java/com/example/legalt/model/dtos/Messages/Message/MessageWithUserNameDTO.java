package com.example.legalt.model.dtos.Messages.Message;

import lombok.Data;

@Data
public class MessageWithUserNameDTO {
    private int id;
    private String comment;
    private String userName;
    private int project;
}
