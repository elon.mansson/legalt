package com.example.legalt.model.dtos.Messages.Message;

import lombok.Data;

@Data
public class MessageDTO {
    private int id;
    private String comment;
    private String loggedInUser;
    private int project;
}
