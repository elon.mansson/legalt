package com.example.legalt.model.dtos.LoggedInUser;

import lombok.Data;

@Data
public class CreateLoggedInUserDTO {
    private String name;
    private String description;
    private boolean show;
}
