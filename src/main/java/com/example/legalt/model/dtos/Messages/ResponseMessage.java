package com.example.legalt.model.dtos.Messages;

import lombok.Data;

@Data
public class ResponseMessage {
    private String message;
}
