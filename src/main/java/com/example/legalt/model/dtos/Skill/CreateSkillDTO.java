package com.example.legalt.model.dtos.Skill;

import lombok.Data;

@Data
public class CreateSkillDTO {
    private String title;
}
