package com.example.legalt.model.dtos.Project;

import com.example.legalt.model.dtos.Photo.PhotoDTO;
import lombok.Data;

import java.util.Set;

@Data
public class ProjectDTO {
    private int id;
    private String title;
    private String desc;
    private String tag;
    private String gitRepo;
    private Set<String> loggedInUsers;
    private Set<Integer> messages;
    private Set<PhotoDTO> photos;
    private String admin;
}
