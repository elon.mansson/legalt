package com.example.legalt.model.dtos.Messages.Message;

import lombok.Data;

@Data
public class CreateMessageDTO {
    private String comment;
    private String loggedInUser;
}
