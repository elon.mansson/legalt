package com.example.legalt.model.dtos.Accomplishment;

import com.example.legalt.model.LoggedInUser;
import lombok.Data;

import java.util.Set;

@Data
public class AccomplishmentDTO {
    private int id;
    private String url;
}
