package com.example.legalt.model.dtos.Project;

import lombok.Data;

import java.util.Set;

@Data
public class CreateProjectDTO {
    private String title;
    private String desc;
    private String tag;
    private String gitRepo;
    private String admin;
    private Set<String> loggedInUsers;
}
