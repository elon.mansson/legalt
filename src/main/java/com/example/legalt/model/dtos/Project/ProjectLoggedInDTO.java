package com.example.legalt.model.dtos.Project;

import com.example.legalt.model.dtos.LoggedInUser.UserRequestDTO;
import com.example.legalt.model.dtos.Messages.Message.MessageWithUserNameDTO;
import com.example.legalt.model.dtos.Photo.PhotoDTO;
import lombok.Data;

import java.util.Set;

@Data
public class ProjectLoggedInDTO {
    private int id;
    private String title;
    private String desc;
    private String tag;
    private String admin;
    private String gitRepo;
    private Set<String> loggedInUsers;
    private Set<MessageWithUserNameDTO> messages;
    private Set<PhotoDTO> photos;
    private Set<UserRequestDTO> requestUsers;
    private Set<HistoryProjectDTO> history;
}
