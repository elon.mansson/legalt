package com.example.legalt.model.dtos.Project;

import lombok.Data;

@Data
public class UpdateProjectPutDTO {
    private String title;
    private String desc;
    private String tag;
    private String gitRepo;
}
