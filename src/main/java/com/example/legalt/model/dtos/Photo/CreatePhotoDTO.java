package com.example.legalt.model.dtos.Photo;

import lombok.Data;

@Data
public class CreatePhotoDTO {
    private String url;
    private int project;
}
