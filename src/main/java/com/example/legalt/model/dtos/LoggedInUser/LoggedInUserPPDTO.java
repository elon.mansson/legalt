package com.example.legalt.model.dtos.LoggedInUser;

import com.example.legalt.model.dtos.Accomplishment.AccomplishmentDTO;
import com.example.legalt.model.dtos.Skill.SkillPPDTO;
import lombok.Data;

import java.util.Set;

@Data
public class LoggedInUserPPDTO {
    private String name;
    private String description;
    private boolean show;
    private Set<AccomplishmentDTO> accomplishments;
    private Set<SkillPPDTO> skills;
}