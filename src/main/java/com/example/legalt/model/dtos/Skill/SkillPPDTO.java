package com.example.legalt.model.dtos.Skill;

import lombok.Data;

@Data
public class SkillPPDTO {
    private int id;
    private String title;
}
