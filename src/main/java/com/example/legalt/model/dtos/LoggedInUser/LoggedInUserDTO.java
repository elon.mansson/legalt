package com.example.legalt.model.dtos.LoggedInUser;

import com.example.legalt.model.dtos.Skill.SkillDTO;
import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Set;

@Data
public class LoggedInUserDTO {
    private String uid;
    private String name;
    private String description;
    private boolean show;
    private Set<Integer> messages;
    private Set<Integer> projects;
    private Set<SkillDTO> skills;
    private Set<Integer> adminProjects;
    private Set<Integer> accomplishments;
}