package com.example.legalt.model.dtos.Project;

import com.example.legalt.model.dtos.Photo.PhotoDTO;
import lombok.Data;

import java.util.Set;

@Data
public class ProjectNotLoggedInDTO {
    private int id;
    private String title;
    private String desc;
    private String tag;
    private String admin;
    private String gitRepo;
    private Set<PhotoDTO> photos;
}
