package com.example.legalt.model.dtos.Project;

import lombok.Data;

@Data
public class UpdateProjectAdminDTO {
    private int id;
    private String title;
    private String desc;
    private String gitRepo;
}