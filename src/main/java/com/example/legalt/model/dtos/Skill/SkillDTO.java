package com.example.legalt.model.dtos.Skill;

import lombok.Data;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Set;

@Data
public class SkillDTO {
    private int id;
    private String title;
    private Set<String> loggedInUsers;
}
