package com.example.legalt.model.dtos.Project;

import lombok.Data;
import java.util.Set;

@Data
public class DeleteProjectDTO {
    private int id;
    private String admin;
    private Set<String> loggedInUsers;
    private Set<Integer> photos;
    private Set<String> requestUsers;
    private Set<Integer> messages;
    private Set<Integer> history;
}
