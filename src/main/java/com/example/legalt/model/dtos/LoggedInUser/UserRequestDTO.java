package com.example.legalt.model.dtos.LoggedInUser;

import lombok.Data;

@Data
public class UserRequestDTO {
    private String uid;
    private String name;
}
