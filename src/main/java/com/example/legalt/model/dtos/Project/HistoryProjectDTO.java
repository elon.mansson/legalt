package com.example.legalt.model.dtos.Project;

import lombok.Data;

@Data
public class HistoryProjectDTO {
    private int id;
    private String loggedInUser;
    private ProjectNotLoggedInDTO project;
}