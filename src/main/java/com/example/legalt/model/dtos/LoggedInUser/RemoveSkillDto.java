package com.example.legalt.model.dtos.LoggedInUser;

import lombok.Data;

import java.util.Set;

@Data
public class RemoveSkillDto {
    private String uid;
    private Set<Integer> skills;

}
