package com.example.legalt.model.dtos.Project;

import lombok.Data;

import java.util.Set;

@Data
public class ProjectChangeRequestDTO {
    private Set<String> requestUsers;
    private boolean accept;
}
