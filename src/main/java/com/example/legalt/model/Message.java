package com.example.legalt.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "message_comment", nullable = false)
    private String comment;
    @ManyToOne
    @JoinColumn(name = "logged_in_user_uid", nullable = true)
    private LoggedInUser loggedInUser;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;


}
