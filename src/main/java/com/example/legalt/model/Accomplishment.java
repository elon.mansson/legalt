package com.example.legalt.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Accomplishment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "accomplishment_url", nullable = false)
    private String url;

    @ManyToOne
    @JoinColumn(name = "logged_in_user_uid", nullable = true)
    private LoggedInUser loggedInUser;
}
