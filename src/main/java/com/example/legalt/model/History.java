package com.example.legalt.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "history_id", nullable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "logged_in_user_uid", nullable = true)
    private LoggedInUser loggedInUser;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = true)
    private Project project;

    public History(LoggedInUser loggedInUser, Project project) {
        this.loggedInUser = loggedInUser;
        this.project = project;
    }

    public History() {

    }
}
