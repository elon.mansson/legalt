package com.example.legalt.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class LoggedInUser {
    @Id
    @Column(name = "logged_in_user_uid", nullable = false)
    private String uid;


    @Column(name = "logged_in_user_name", length = 40, nullable = false)
    private String name;
    @Column(name = "logged_in_user_description")
    private String description;
    @Column(name = "logged_in_user_show")
    private Boolean show = false;

    @ManyToMany
    @JoinTable(
            name = "logged_in_user_project",
            joinColumns = {@JoinColumn(name = "logged_in_user_uid")},
            inverseJoinColumns = {@JoinColumn(name = "project_id")}
    )
    private Set<Project> projects;

    @ManyToMany
    @JoinTable(
            name = "logged_in_user_skill",
            joinColumns = {@JoinColumn(name = "skill_id")},
            inverseJoinColumns = {@JoinColumn(name = "loggedInUser_uid")}
    )
    private Set<Skill> skills;

    @ManyToMany
    @JoinTable(
            name = "logged_in_user_requests",
            joinColumns = {@JoinColumn(name = "project_id")},
            inverseJoinColumns = {@JoinColumn(name = "logged_in_user_uid")}
    )
    private Set<Project> requests;

    @OneToMany(mappedBy = "loggedInUser")
    private Set<Message> messages;

    @OneToMany(mappedBy = "admin")
    private Set<Project> adminProjects;

    @OneToMany(mappedBy = "loggedInUser")
    private Set<Accomplishment> accomplishments;

    @OneToMany(mappedBy = "loggedInUser")
    private Set<History> history;

}