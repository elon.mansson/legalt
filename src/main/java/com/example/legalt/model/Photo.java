package com.example.legalt.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Photo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "photo_url")
    private String url;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
}
