package com.example.legalt.model;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "project_title", length = 100, nullable = false)
    private String title;
    @Column(name = "project_desc", length = 255, nullable = false)
    private String desc;
    @Column(name = "project_tag", length = 40)
    private String tag;
    @Column(name = "project_git_repo")
    private String gitRepo;

    @ManyToMany(mappedBy = "projects")
    private Set<LoggedInUser> loggedInUsers;

    @OneToMany(mappedBy = "project")
    private Set<Message> messages;

    @OneToMany(mappedBy = "project")
    private Set<Photo> photos;

    @OneToMany(mappedBy = "project")
    private Set<History> history;

    @ManyToMany(mappedBy = "requests")
    private Set<LoggedInUser> requestUsers;

    @ManyToOne
    @JoinColumn(name = "logged_in_user_uid", nullable = true)
    private LoggedInUser admin;


}