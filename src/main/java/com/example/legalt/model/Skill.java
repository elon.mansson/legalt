package com.example.legalt.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "skill_title", length = 40, nullable = false)
    private String title;

    @ManyToMany(mappedBy = "skills")
    private Set<LoggedInUser> loggedInUsers;
}