package com.example.legalt.service.Message;

import com.example.legalt.model.Message;
import com.example.legalt.service.CrudService;

import javax.persistence.criteria.CriteriaBuilder;

public interface MessageService extends CrudService<Message, Integer> {

}
