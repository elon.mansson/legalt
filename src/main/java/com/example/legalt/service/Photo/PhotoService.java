package com.example.legalt.service.Photo;

import com.example.legalt.model.Photo;
import com.example.legalt.service.CrudService;

public interface PhotoService extends CrudService<Photo, Integer> {
}
