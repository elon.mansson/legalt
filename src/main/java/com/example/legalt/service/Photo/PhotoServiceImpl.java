package com.example.legalt.service.Photo;

import com.example.legalt.model.Photo;
import com.example.legalt.repository.PhotoRepository;
import com.example.legalt.service.exceptions.PhotoNotFoundException;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PhotoServiceImpl implements PhotoService{

    private PhotoRepository photoRepository;

    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    public Photo findById(Integer id) {
        return photoRepository.findById(id)
                .orElseThrow(() -> new PhotoNotFoundException(id));
    }

    @Override
    public Collection<Photo> findAll() {
        return photoRepository.findAll();
    }

    @Override
    public Photo add(Photo photo) {
        return photoRepository.save(photo);
    }

    @Override
    public Photo update(Photo photo) {
        return photoRepository.save(photo);
    }

    @Override
    public void deleteById(Integer id) {
        photoRepository.deleteById(id);
    }
}
