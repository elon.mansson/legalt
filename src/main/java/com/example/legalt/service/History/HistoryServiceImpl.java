package com.example.legalt.service.History;

import com.example.legalt.model.History;
import com.example.legalt.repository.HistoryRepository;
import com.example.legalt.service.exceptions.ProjectNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class HistoryServiceImpl implements HistoryService{

    private HistoryRepository historyRepository;

    public HistoryServiceImpl(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    @Override
    public History findById(Integer id) {
        return historyRepository.findById(id)
                .orElseThrow(() -> new ProjectNotFoundException(id));
    }

    @Override
    public Collection<History> findAll() {
        return historyRepository.findAll();
    }

    @Override
    public History add(History entity) {
        return historyRepository.save(entity);
    }

    @Override
    public History update(History entity) {
        return historyRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        historyRepository.deleteById(integer);
    }
}
