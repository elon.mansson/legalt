package com.example.legalt.service.History;

import com.example.legalt.model.History;
import com.example.legalt.service.CrudService;

public interface HistoryService extends CrudService<History, Integer> {
}
