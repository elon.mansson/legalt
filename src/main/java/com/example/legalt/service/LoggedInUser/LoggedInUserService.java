package com.example.legalt.service.LoggedInUser;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.service.CrudService;

public interface LoggedInUserService extends CrudService<LoggedInUser, String>  {
    LoggedInUser add(String uid, String name) throws Exception;
    LoggedInUser add(LoggedInUser user);


}
