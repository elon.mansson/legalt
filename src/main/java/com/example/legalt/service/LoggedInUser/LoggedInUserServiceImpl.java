package com.example.legalt.service.LoggedInUser;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.repository.LoggedInUserRepository;
import com.example.legalt.service.exceptions.UserNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class LoggedInUserServiceImpl implements LoggedInUserService{
    private LoggedInUserRepository loggedInUserRepository;

    public LoggedInUserServiceImpl(LoggedInUserRepository loggedInUserRepository) {

        this.loggedInUserRepository = loggedInUserRepository;
    }
    @Override
    public LoggedInUser add(String uid,String name)  {
        // Prevents internal server error for duplicates
        if(loggedInUserRepository.existsById(uid))
            new Exception();
        // Create new user
        LoggedInUser user = new LoggedInUser();
        user.setUid(uid);
        user.setName(name);
        return loggedInUserRepository.save(user);
    }

    @Override
    public LoggedInUser findById(String uid) {
        return loggedInUserRepository.findById(uid)
                .orElseThrow(() -> new UserNotFoundException(uid));
    }

    @Override
    public Collection<LoggedInUser> findAll() {
        return loggedInUserRepository.findAll();
    }

    @Override
    public LoggedInUser add(LoggedInUser loggedInUser) {
        return loggedInUserRepository.save(loggedInUser);
    }

    @Override
    public LoggedInUser update(LoggedInUser loggedInUser) {
        return loggedInUserRepository.save(loggedInUser);
    }

    @Override
    public void deleteById(String s) {

    }



}
