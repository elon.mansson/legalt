package com.example.legalt.service.Skill;

import com.example.legalt.model.Skill;
import com.example.legalt.repository.SkillRepository;
import com.example.legalt.service.exceptions.SkillNotFoundException;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SkillServiceImpl implements SkillService{

    private SkillRepository skillRepository;

    public SkillServiceImpl(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Override
    public Skill findById(Integer id) {
        return skillRepository.findById(id)
                .orElseThrow(() -> new SkillNotFoundException(id));
    }

    @Override
    public Collection<Skill> findAll() {
        return skillRepository.findAll();
    }

    @Override
    public Skill add(Skill skill) {
        return skillRepository.save(skill);
    }

    @Override
    public Skill update(Skill skill) {
        return skillRepository.save(skill);
    }

    @Override
    public void deleteById(Integer id) {
        skillRepository.deleteById((id));
    }
}
