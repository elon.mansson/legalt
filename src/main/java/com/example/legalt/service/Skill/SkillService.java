package com.example.legalt.service.Skill;

import com.example.legalt.model.Skill;
import com.example.legalt.service.CrudService;

public interface SkillService extends CrudService<Skill, Integer> {
}
