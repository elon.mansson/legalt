package com.example.legalt.service.Project;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Project;
import com.example.legalt.service.CrudService;

public interface ProjectService extends CrudService<Project, Integer> {
}
