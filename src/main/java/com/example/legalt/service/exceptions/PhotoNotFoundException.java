package com.example.legalt.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PhotoNotFoundException extends RuntimeException{
    public PhotoNotFoundException(int id) {
        super("No photo found with id: " + id);
    }
}
