package com.example.legalt.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class SkillNotFoundException extends RuntimeException{
    public SkillNotFoundException(int id) {
        super("No skill found with id " + id);
    }
}
