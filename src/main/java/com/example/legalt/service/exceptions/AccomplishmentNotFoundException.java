package com.example.legalt.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class AccomplishmentNotFoundException extends RuntimeException {
    public AccomplishmentNotFoundException(int id) {
        super("No accomplishment found with id: " + id);
    }
}
