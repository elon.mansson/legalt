package com.example.legalt.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MessageNotFoundException extends RuntimeException{
    public MessageNotFoundException(int id) {
        super("No message found with id: " + id);
    }
}
