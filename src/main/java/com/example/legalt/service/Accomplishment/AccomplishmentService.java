package com.example.legalt.service.Accomplishment;

import com.example.legalt.model.Accomplishment;
import com.example.legalt.service.CrudService;

public interface AccomplishmentService extends CrudService<Accomplishment, Integer> {
}
