package com.example.legalt.service.Accomplishment;

import com.example.legalt.model.Accomplishment;
import com.example.legalt.repository.AccomplishmentRepository;
import com.example.legalt.repository.LoggedInUserRepository;
import com.example.legalt.service.exceptions.AccomplishmentNotFoundException;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class AccomplishmentServiceImpl implements AccomplishmentService{

    private AccomplishmentRepository accomplishmentRepository;

    public AccomplishmentServiceImpl(AccomplishmentRepository accomplishmentRepository) {
        this.accomplishmentRepository = accomplishmentRepository;
    }

    @Override
    public Accomplishment findById(Integer id) {
        return accomplishmentRepository.findById(id)
                .orElseThrow(() -> new AccomplishmentNotFoundException(id));
    }

    @Override
    public Collection<Accomplishment> findAll() {
        return accomplishmentRepository.findAll();
    }

    @Override
    public Accomplishment add(Accomplishment accomplishment) {
        return accomplishmentRepository.save(accomplishment);
    }

    @Override
    public Accomplishment update(Accomplishment accomplishment) {
        return accomplishmentRepository.save(accomplishment);
    }

    @Override
    public void deleteById(Integer id) {
        accomplishmentRepository.deleteById(id);
    }
}
