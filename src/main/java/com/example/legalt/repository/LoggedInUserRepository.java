package com.example.legalt.repository;

import com.example.legalt.model.LoggedInUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoggedInUserRepository extends JpaRepository<LoggedInUser, String> {
}
