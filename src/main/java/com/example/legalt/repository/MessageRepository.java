package com.example.legalt.repository;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
}
