package com.example.legalt.repository;

import com.example.legalt.model.Accomplishment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccomplishmentRepository extends JpaRepository<Accomplishment, Integer> {
}
