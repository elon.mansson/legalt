package com.example.legalt.mappers.history;

import com.example.legalt.mappers.project.ProjectMapperNotLoggedIn;
import com.example.legalt.model.History;
import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Project.HistoryProjectDTO;
import com.example.legalt.model.dtos.Project.ProjectNotLoggedInDTO;
import com.example.legalt.service.History.HistoryService;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
@Mapper(componentModel = "spring")
public abstract class HistoryMapper {

    @Autowired
    protected HistoryService historyService;
    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected LoggedInUserService loggedInUserService;
    @Autowired
    protected ProjectMapperNotLoggedIn projectMapperNotLoggedIn;

    @Mapping(target = "loggedInUser", source = "loggedInUser.uid")
    @Mapping(target = "project", source = "project", qualifiedByName = "projectToProjectDto")
    public abstract HistoryProjectDTO historyToHistoryDto(History history);

    public abstract Collection<HistoryProjectDTO> HistoryToHistoryDto(Collection<History> histories);

    @Mapping(target = "loggedInUser", source = "loggedInUser", qualifiedByName = "userToId")
    @Mapping(target = "project", source = "project", qualifiedByName = "projectDtoToProject")
    public abstract History historyDtoToHistory(HistoryProjectDTO dto);

    public abstract Collection<History> historyDtoToHistory(Collection<HistoryProjectDTO> historyProjectDTOS);

    @Named("projectDtoToProject")
    Project projectDtoToProject(ProjectNotLoggedInDTO dto){return projectService.findById(dto.getId());}

    @Named("userToId")
    LoggedInUser mapUserToId(String uid) {
        return loggedInUserService.findById(uid);
    }

    @Named("projectToProjectDto")
    ProjectNotLoggedInDTO mapProjectToDto(Project source) {
        return projectMapperNotLoggedIn.projectToProjectDto(source);
    }
}
