package com.example.legalt.mappers.photo;
import com.example.legalt.model.Photo;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Photo.PhotoDTO;
import com.example.legalt.service.Photo.PhotoService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class PhotoMapper {

    @Autowired
    protected PhotoService photoService;
    @Autowired
    protected ProjectService projectService;

    @Mapping(target = "project", source = "project.id")
    public abstract PhotoDTO photoToPhotoDto(Photo photo);

    public abstract Collection<PhotoDTO> photoToPhotoDto(Collection<Photo> photos);

    @Mapping(target = "project", source = "project", qualifiedByName = "projectToId")
    public abstract Photo photoDtoToPhoto(PhotoDTO dto);

    public abstract Collection<Photo> photoDtoToPhoto(Collection<PhotoDTO> photoDTOS);

    @Named("projectToId")
    Project mapPhotoId(int id) {
        return projectService.findById(id);
    }
}
