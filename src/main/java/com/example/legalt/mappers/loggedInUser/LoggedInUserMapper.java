package com.example.legalt.mappers.loggedInUser;

import com.example.legalt.mappers.skill.SkillMapper;
import com.example.legalt.model.*;
import com.example.legalt.model.dtos.LoggedInUser.CreateLoggedInUserDTO;
import com.example.legalt.model.dtos.LoggedInUser.LoggedInUserDTO;
import com.example.legalt.model.dtos.Skill.SkillDTO;
import com.example.legalt.service.Accomplishment.AccomplishmentService;
import com.example.legalt.service.Message.MessageService;
import com.example.legalt.service.Project.ProjectService;
import com.example.legalt.service.Skill.SkillService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class LoggedInUserMapper {
    @Autowired
    protected MessageService messageService;
    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected SkillService skillService;
    @Autowired
    protected SkillMapper skillMapper;
    @Autowired
    protected AccomplishmentService accomplishmentService;

    @Mapping(target = "accomplishments", source = "accomplishments", qualifiedByName = "accomplishmentToAccomplishmentDTO")
    @Mapping(target = "messages", source = "messages", qualifiedByName = "messageToMessageDTO")
    @Mapping(target = "projects", source = "projects", qualifiedByName = "projectToProjectDTO")
    @Mapping(target= "skills", source = "skills", qualifiedByName = "skillToSkillDTO")
    @Mapping(target = "adminProjects", source = "adminProjects", qualifiedByName = "adminProjectsToAdminProjectsDTO")
    public abstract LoggedInUserDTO loggedInUserToLoggedInUserDTO(LoggedInUser loggedInUser);

    public abstract Collection<LoggedInUserDTO> loggedInUserToLoggedInUserDTO(Collection<LoggedInUser> loggedInUsers);

    @Mapping(target = "accomplishments", source = "accomplishments", qualifiedByName = "accomplishmentDtoToAccomplishment")
    @Mapping(target = "messages", source = "messages", qualifiedByName = "messageDtoToMessage")
    @Mapping(target = "skills", source = "skills", qualifiedByName = "skillDtoToSkill")
    @Mapping(target = "projects", source = "projects", qualifiedByName = "ProjectDtoToProject")
    @Mapping(target = "adminProjects", source = "adminProjects", qualifiedByName = "adminProjectsDtoToAdminProjects")
    public abstract LoggedInUser loggedInUserDtoToLoggedInUser(LoggedInUserDTO dto);

    @Mapping(target = "accomplishments", ignore = true)
    @Mapping(target = "messages", ignore = true)
    @Mapping(target = "skills", ignore = true)
    @Mapping(target = "projects", ignore = true)
    @Mapping(target = "adminProjects", ignore = true)
    public abstract LoggedInUser loggedInUserDtoToLoggedInUser(CreateLoggedInUserDTO dto);

    public abstract Collection<LoggedInUser> loggedInUserDtoToLoggedInUser(Collection<LoggedInUserDTO> loggedInUser);

    @Named("messageDtoToMessage")
    Message mapIdToMessage(int id) {
        return messageService.findById(id);
    }
    @Named("ProjectDtoToProject")
    Project mapIdToProject(int id){return projectService.findById(id);}

    @Named("skillDtoToSkill")
    Skill mapIdToSkill(SkillDTO dto){return skillService.findById(dto.getId());}

    @Named("adminProjectsDtoToAdminProjects")
    Project mapIdToAdminProject(int id) {
        return projectService.findById(id);
    }
    @Named("accomplishmentDtoToAccomplishment")
    Accomplishment mapIdToAccomplishment(int id) {
        return accomplishmentService.findById(id);
    }

    @Named("messageToMessageDTO")
    Set<Integer> mapMessagesToIds(Set<Message> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Named("projectToProjectDTO")
    Set<Integer> mapProjectsToIds(Set<Project> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Named("skillToSkillDTO")
    Set<SkillDTO> mapSkillsToIds(Set<Skill> source) {
        var dto = new HashSet<SkillDTO>();
         source.forEach(e -> dto.add(skillMapper.skillToSkillDTO(e)));
         return dto;
    }
    @Named("adminProjectsToAdminProjectsDTO")
    Set<Integer> mapAdminProjectToIds(Set<Project> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
    @Named("accomplishmentToAccomplishmentDTO")
    Set<Integer> mapAccomplishmentsToIds(Set<Accomplishment> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
}
