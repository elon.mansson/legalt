package com.example.legalt.mappers.loggedInUser;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.dtos.LoggedInUser.UpdateLoggedInUserDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Skill.SkillService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class UpdateUserMapper {
    @Autowired
    protected LoggedInUserService loggedInUserService;
    @Autowired
    protected SkillService skillService;

    public abstract UpdateLoggedInUserDTO UpdateloggedInUserToLoggedInUserDTO(LoggedInUser loggedInUser);


    public abstract LoggedInUser UpdateloggedInUserDtoToLoggedInUser(UpdateLoggedInUserDTO dto);




}
