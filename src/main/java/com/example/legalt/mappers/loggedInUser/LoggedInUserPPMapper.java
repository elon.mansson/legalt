package com.example.legalt.mappers.loggedInUser;

import com.example.legalt.mappers.accomplishment.AccomplishmentMapper;
import com.example.legalt.mappers.skill.SkillPPMapper;
import com.example.legalt.model.Accomplishment;
import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Skill;
import com.example.legalt.model.dtos.Accomplishment.AccomplishmentDTO;
import com.example.legalt.model.dtos.LoggedInUser.LoggedInUserPPDTO;
import com.example.legalt.model.dtos.Skill.SkillPPDTO;
import com.example.legalt.service.Accomplishment.AccomplishmentService;
import com.example.legalt.service.Skill.SkillService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class LoggedInUserPPMapper {

    @Autowired
    protected SkillService skillService;
    @Autowired
    protected SkillPPMapper skillPPMapper;
    @Autowired
    protected AccomplishmentService accomplishmentService;
    @Autowired
    protected AccomplishmentMapper accomplishmentMapper;

    @Mapping(target = "accomplishments", source = "accomplishments", qualifiedByName = "accomplishmentToAccomplishmentDTO")
    @Mapping(target= "skills", source = "skills", qualifiedByName = "skillToSkillDTO")
    public abstract LoggedInUserPPDTO loggedInUserToLoggedInUserDTO(LoggedInUser loggedInUser);

    public abstract Collection<LoggedInUserPPDTO> loggedInUserToLoggedInUserDTO(Collection<LoggedInUser> loggedInUsers);

    @Mapping(target = "skills", source = "skills", qualifiedByName = "skillDtoToSkill")
    @Mapping(target = "accomplishments", source = "accomplishments", qualifiedByName = "accomplishmentDtoToAccomplishment")
    public abstract LoggedInUser loggedInUserDtoToLoggedInUser(LoggedInUserPPDTO dto);

    public abstract Collection<LoggedInUser> loggedInUserDtoToLoggedInUser(Collection<LoggedInUserPPDTO> loggedInUser);

    @Named("skillDtoToSkill")
    Skill mapIdToSkill(SkillPPDTO dto){return skillService.findById(dto.getId());}

    @Named("accomplishmentDtoToAccomplishment")
    Accomplishment mapIdToAccomplishment(AccomplishmentDTO dto) {
        return accomplishmentService.findById(dto.getId());
    }

    @Named("skillToSkillDTO")
    Set<SkillPPDTO> mapSkillsToIds(Set<Skill> source) {
        var dtos = new HashSet<SkillPPDTO>();
        source.forEach(e -> dtos.add(skillPPMapper.skillToSkillDTO(e)));
        return dtos;
    }

    @Named("accomplishmentToAccomplishmentDTO")
    Set<AccomplishmentDTO> mapAccomplishmentsToIds(Set<Accomplishment> source) {
        var dtos = new HashSet<AccomplishmentDTO>();
        source.forEach(e -> dtos.add(accomplishmentMapper.accomplishmentToAccomplishmentDto(e)));
        return dtos;
    }
}
