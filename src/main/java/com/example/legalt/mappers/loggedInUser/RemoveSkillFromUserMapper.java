package com.example.legalt.mappers.loggedInUser;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Skill;
import com.example.legalt.model.dtos.LoggedInUser.RemoveSkillDto;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Skill.SkillService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class RemoveSkillFromUserMapper {

    @Autowired
    protected LoggedInUserService loggedInUserService;

    @Autowired
    protected SkillService skillService;


    @Mapping(target= "skills", source = "skills", qualifiedByName = "skillToSkillDTO")
    public abstract RemoveSkillDto loggedInUserToLoggedInUserDTO(LoggedInUser loggedInUser);

    public abstract Collection<RemoveSkillDto> loggedInUserToLoggedInUserDTO(Collection<LoggedInUser> loggedInUsers);

    @Mapping(target = "skills", source = "skills", qualifiedByName = "skillDtoToSkill")
    public abstract LoggedInUser loggedInUserDtoToLoggedInUser(RemoveSkillDto dto);

    public abstract Collection<LoggedInUser> loggedInUserDtoToLoggedInUser(Collection<RemoveSkillDto> skillDTOS);

    @Named("skillDtoToSkill")
    Skill mapIdToSkill(int id){return skillService.findById(id);}


    @Named("skillToSkillDTO")
    Set<Integer> mapSkillsToIds(Set<Skill> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }


}
