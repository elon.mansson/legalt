package com.example.legalt.mappers.loggedInUser;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.dtos.LoggedInUser.UserRequestDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class UserRequestMapper {

    @Autowired
    protected LoggedInUserService loggedInUserService;

    public abstract UserRequestDTO userToUserDto(LoggedInUser loggedInUser);

    public abstract Collection<UserRequestDTO> userToUserDto(Collection<LoggedInUser> loggedInUsers);

    public abstract LoggedInUser userDtoToUser(UserRequestDTO dto);

    public abstract Collection<LoggedInUser> userDtoToUser(Collection<UserRequestDTO> userDTOS);
}
