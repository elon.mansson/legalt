package com.example.legalt.mappers.skill;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Skill;
import com.example.legalt.model.dtos.Skill.CreateSkillDTO;
import com.example.legalt.model.dtos.Skill.SkillDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Skill.SkillService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class SkillMapper {

    @Autowired
    protected SkillService skillService;
    @Autowired
    protected LoggedInUserService loggedInUserService;

    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "SkillToSkillDto")
    public abstract SkillDTO skillToSkillDTO(Skill skill);

    public abstract Collection<SkillDTO> skillToSkillDTO(Collection<Skill> skills);

    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "SkillDtoToSkill")
    public abstract Skill skillDtoToSkill(SkillDTO dto);

    @Mapping(target = "loggedInUsers", ignore = true)
    public abstract Skill skillDtoToSkill(CreateSkillDTO dto);

    public abstract Collection<Skill> skillDtoToSkill(Collection<SkillDTO> skillDTOS);

    @Named("SkillDtoToSkill")
    LoggedInUser mapIdToSkill(String uid){return loggedInUserService.findById(uid);}

    @Named("SkillToSkillDto")
    Set<String> mapSkillsToIds(Set<LoggedInUser> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getUid()).collect(Collectors.toSet());
    }
}
