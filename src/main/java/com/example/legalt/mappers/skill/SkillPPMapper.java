package com.example.legalt.mappers.skill;

import com.example.legalt.model.Skill;
import com.example.legalt.model.dtos.Skill.CreateSkillDTO;
import com.example.legalt.model.dtos.Skill.SkillPPDTO;
import com.example.legalt.service.Skill.SkillService;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class SkillPPMapper {

    @Autowired
    protected SkillService skillService;


    public abstract SkillPPDTO skillToSkillDTO(Skill skill);

    public abstract Collection<SkillPPDTO> skillToSkillDTO(Collection<Skill> skills);

    public abstract Skill skillDtoToSkill(SkillPPDTO dto);

    public abstract Skill skillDtoToSkill(CreateSkillDTO dto);

    public abstract Collection<Skill> skillDtoToSkill(Collection<SkillPPDTO> skillDTOS);
}