package com.example.legalt.mappers.message;

import com.example.legalt.model.Message;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Messages.Message.MessageWithUserNameDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Message.MessageService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class MessageWithUserNameMapper {

    @Autowired
    protected MessageService messageService;
    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected LoggedInUserService loggedInUserService;

    @Mapping(target = "userName", source = "loggedInUser.name")
    @Mapping(target = "project", source = "project.id")
    public abstract MessageWithUserNameDTO messageToMessageDTO(Message message);

    public abstract Collection<MessageWithUserNameDTO> messageToMessageDTO(Collection<Message> messages);

    @Mapping(target = "loggedInUser.name", source = "userName", qualifiedByName = "userDtoToUser")
    @Mapping(target = "project", source = "project", qualifiedByName = "projectIdToProjects")
    public abstract Message messageDtoToMessage(MessageWithUserNameDTO dto);

    @Named("userDtoToUser")
    String nameToUserName(String uid){return loggedInUserService.findById(uid).getName();}

    @Named("projectIdToProjects")
    Project idToProject(int id){return projectService.findById(id);}

    @Named("messageToMessageDto")
    Set<MessageWithUserNameDTO> mapMessagesToIds(Set<Message> source) {
        if(source == null){
            return null;
        }
        return (Set<MessageWithUserNameDTO>) messageToMessageDTO(source);
    }
}