package com.example.legalt.mappers.message;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Message;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Messages.Message.CreateMessageDTO;
import com.example.legalt.model.dtos.Messages.Message.MessageDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Message.MessageService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class MessageMapper {

    @Autowired
    protected MessageService messageService;
    @Autowired
    protected LoggedInUserService loggedInUserService;
    @Autowired
    protected ProjectService projectService;

    @Mapping(target= "loggedInUser", source = "loggedInUser.uid")
    @Mapping(target = "project", source = "project.id")
    public abstract MessageDTO messageToMessageDTO(Message message);

    public abstract Collection<MessageDTO> messageToMessageDTO(Collection<Message> messages);

    @Mapping(target= "loggedInUser", source = "loggedInUser", qualifiedByName = "loggedInUserIdToLoggedInUser")
    @Mapping(target = "project", source = "project", qualifiedByName = "projectIdToProjects")
    public abstract Message messageDtoToMessage(MessageDTO dto);

    @Mapping(target = "loggedInUser", ignore = true)
    @Mapping(target = "project", ignore = true)
    public abstract Message messageDtoToMessage(CreateMessageDTO dto);

    @Named("loggedInUserIdToLoggedInUser")
    LoggedInUser IdToLoggedInUser(String uid){return loggedInUserService.findById(uid);}

    @Named("projectIdToProjects")
    Project IdToProject(int id){return projectService.findById(id);}
}
