package com.example.legalt.mappers.accomplishment;

import com.example.legalt.model.Accomplishment;
import com.example.legalt.model.dtos.Accomplishment.AccomplishmentDTO;
import com.example.legalt.service.Accomplishment.AccomplishmentService;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class AccomplishmentMapper {

    @Autowired
    protected AccomplishmentService accomplishmentService;
    @Autowired
    protected LoggedInUserService loggedInUserService;

    public abstract AccomplishmentDTO accomplishmentToAccomplishmentDto(Accomplishment accomplishment);

    public abstract Collection<AccomplishmentDTO> accomplishmentToAccomplishmentDto(Collection<Accomplishment> accomplishments);

    public abstract Accomplishment accomplishmentDtoToAccomplishment(AccomplishmentDTO dto);

    public abstract Collection<Accomplishment> accomplishmentDtoToAccomplishment(Collection<AccomplishmentDTO> accomplishmentDTOS);
}