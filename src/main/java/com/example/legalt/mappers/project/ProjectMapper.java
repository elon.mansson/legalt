package com.example.legalt.mappers.project;

import com.example.legalt.mappers.photo.PhotoMapper;
import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Message;
import com.example.legalt.model.Photo;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Photo.PhotoDTO;
import com.example.legalt.model.dtos.Project.CreateProjectDTO;
import com.example.legalt.model.dtos.Project.ProjectDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Message.MessageService;
import com.example.legalt.service.Photo.PhotoService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ProjectMapper {

    @Autowired
    protected LoggedInUserService loggedInUserService;

    @Autowired
    protected ProjectService projectService;

    @Autowired
    protected MessageService messageService;
    @Autowired
    protected PhotoService photoService;
    @Autowired
    protected PhotoMapper photoMapper;

    @Mapping(target = "photos", source = "photos", qualifiedByName = "photoToPhotoDto")
    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "ProjectToProjectDto")
    @Mapping(target = "admin", source = "admin.uid")
    @Mapping(target = "messages", source = "messages", qualifiedByName = "messageToMessageDto")
    public abstract ProjectDTO projectToProjectDto(Project project);

    public abstract Collection<ProjectDTO> projectToProjectDto(Collection<Project> projects);

    @Mapping(target = "photos", source = "photos", qualifiedByName = "photoDtoToPhoto")
    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "ProjectDtoToProject")
    @Mapping(target = "admin", source = "admin", qualifiedByName = "AdminToId")
    @Mapping(target = "messages", source = "messages", qualifiedByName = "messageDtoToMessage")
    public abstract Project projectDtoToProject(ProjectDTO dto);

    @Mapping(target = "loggedInUsers", ignore = true)
    @Mapping(target = "admin", ignore = true)
    @Mapping(target = "messages", ignore = true)
    public abstract Project projectDtoToProject(CreateProjectDTO dto);

    public abstract Collection<Project> projectDtoToProject(Collection<ProjectDTO> projectDTOS);

    @Named("ProjectDtoToProject")
    LoggedInUser mapIdToProject(String uid){return loggedInUserService.findById(uid);}
    @Named("AdminToId")
    LoggedInUser mapAdminIdToProject(String uid) {
        return loggedInUserService.findById(uid);
    }
    @Named("messageDtoToMessage")
    Message mapMessageIdToProject(int id){return messageService.findById(id);}
    @Named("photoDtoToPhoto")
    Photo mapDtoToPhoto(PhotoDTO dto) {
        return photoService.findById(dto.getId());
    }

    @Named("ProjectToProjectDto")
    Set<String> mapProjectsToIds(Set<LoggedInUser> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getUid()).collect(Collectors.toSet());
    }
    @Named("messageToMessageDto")
    Set<Integer> mapMessagesToIds(Set<Message> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
    @Named("photoToPhotoDto")
    Set<PhotoDTO> mapPhotoToDto(Set<Photo> source) {
        var dtos = new HashSet<PhotoDTO>();
        source.forEach(e -> dtos.add(photoMapper.photoToPhotoDto(e)));
        return dtos;
    }
}
