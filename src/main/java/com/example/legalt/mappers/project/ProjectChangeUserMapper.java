package com.example.legalt.mappers.project;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Project.ProjectChangeUserDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ProjectChangeUserMapper {

    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected LoggedInUserService loggedInUserService;

    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "ProjectToProjectDto")
    public abstract ProjectChangeUserDTO projectToProjectDto(Project project);

    public abstract Collection<ProjectChangeUserDTO> projectToProjectDto(Collection<Project> projects);

    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "ProjectDtoToProject")
    public abstract Project projectDtoToProject(ProjectChangeUserDTO dto);

    public abstract Collection<Project> projectDtoToProject(Collection<ProjectChangeUserDTO> projectDTOS);

    @Named("ProjectDtoToProject")
    LoggedInUser mapIdToProject(String uid){return loggedInUserService.findById(uid);}

    @Named("ProjectToProjectDto")
    Set<String> mapProjectsToIds(Set<LoggedInUser> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getUid()).collect(Collectors.toSet());
    }
}
