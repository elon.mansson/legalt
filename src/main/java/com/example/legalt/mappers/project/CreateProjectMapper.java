package com.example.legalt.mappers.project;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Project.CreateProjectDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CreateProjectMapper {

    @Autowired
    protected ProjectService projectService;

    @Autowired
    protected LoggedInUserService loggedInUserService;

    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "ProjectToProjectDto")
    @Mapping(target = "admin", source = "admin.uid")
    public abstract CreateProjectDTO projectToProjectDto(Project project);

    public abstract Collection<CreateProjectDTO> projectToProjectDto(Collection<Project> projects);

    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "ProjectDtoToProject")
    @Mapping(target = "admin", source = "admin", qualifiedByName = "AdminToId")
    public abstract Project projectDtoToProject(CreateProjectDTO dto);

    public abstract Collection<Project> projectDtoToProject(Collection<CreateProjectDTO> projectDTOS);

    @Named("ProjectDtoToProject")
    LoggedInUser mapIdToProject(String uid){return loggedInUserService.findById(uid);}
    @Named("AdminToId")
    LoggedInUser mapAdminIdToProject(String uid) {
        return loggedInUserService.findById(uid);
    }

    @Named("ProjectToProjectDto")
    Set<String> mapProjectsToIds(Set<LoggedInUser> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getUid()).collect(Collectors.toSet());
    }
}
