package com.example.legalt.mappers.project;

import com.example.legalt.model.History;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Project.ProjectAddToHistoryDTO;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class ProjectAddToHistoryMapper {


    public abstract ProjectAddToHistoryDTO projectToProjectDto(History history);

    public abstract Collection<ProjectAddToHistoryDTO> projectToProjectDto(Collection<History> history);

    public abstract History projectDtoToProject(ProjectAddToHistoryDTO dto);

    public abstract Collection<History> projectDtoToProject(Collection<ProjectAddToHistoryDTO> historyDTOS);
}
