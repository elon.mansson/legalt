package com.example.legalt.mappers.project;

import com.example.legalt.mappers.photo.PhotoMapper;
import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Photo;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Photo.PhotoDTO;
import com.example.legalt.model.dtos.Project.ProjectNotLoggedInDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Photo.PhotoService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class ProjectMapperNotLoggedIn {
    @Autowired
    protected LoggedInUserService loggedInUserService;
    @Autowired
    protected PhotoService photoService;
    @Autowired
    PhotoMapper photoMapper;


    @Mapping(target = "photos", source = "photos", qualifiedByName = "photoToPhotoDto")
    @Mapping(target = "admin", source = "admin.uid")
    public abstract ProjectNotLoggedInDTO projectToProjectDto(Project project);

    public abstract Collection<ProjectNotLoggedInDTO> projectToProjectDto(Collection<Project> projects);

    @Mapping(target = "photos", source = "photos", qualifiedByName = "photoDtoToPhoto")
    @Mapping(target = "admin", source = "admin", qualifiedByName = "AdminToId")
    public abstract Project projectDtoToProject(ProjectNotLoggedInDTO dto);

    public abstract Collection<Project> projectDtoToProject(Collection<ProjectNotLoggedInDTO> dtos);

    @Named("AdminToId")
    LoggedInUser mapAdminIdToProject(String uid) {
        return loggedInUserService.findById(uid);
    }

    @Named("photoDtoToPhoto")
    Photo mapDtoToPhoto(PhotoDTO dto) {
        return photoService.findById(dto.getId());
    }

    @Named("photoToPhotoDto")
    Set<PhotoDTO> mapPhotoToDto(Set<Photo> source) {
        var dtos = new HashSet<PhotoDTO>();
        source.forEach(e -> dtos.add(photoMapper.photoToPhotoDto(e)));
        return dtos;
    }
}
