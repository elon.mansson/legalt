package com.example.legalt.mappers.project;

import com.example.legalt.mappers.history.HistoryMapper;
import com.example.legalt.mappers.loggedInUser.UserRequestMapper;
import com.example.legalt.mappers.message.MessageWithUserNameMapper;
import com.example.legalt.mappers.photo.PhotoMapper;
import com.example.legalt.model.*;
import com.example.legalt.model.dtos.LoggedInUser.UserRequestDTO;
import com.example.legalt.model.dtos.Messages.Message.MessageWithUserNameDTO;
import com.example.legalt.model.dtos.Photo.PhotoDTO;
import com.example.legalt.model.dtos.Project.HistoryProjectDTO;
import com.example.legalt.model.dtos.Project.ProjectLoggedInDTO;
import com.example.legalt.service.History.HistoryService;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Message.MessageService;
import com.example.legalt.service.Photo.PhotoService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ProjectMapperLoggedIn {

    @Autowired
    protected LoggedInUserService loggedInUserService;
    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected MessageService messageService;
    @Autowired
    protected MessageWithUserNameMapper messageWithUserNameMapper;
    @Autowired
    protected PhotoMapper photoMapper;
    @Autowired
    protected PhotoService photoService;
    @Autowired
    protected UserRequestMapper userRequestMapper;
    @Autowired
    protected HistoryService historyService;
    @Autowired
    protected HistoryMapper historyMapper;


    @Mapping(target = "history", source = "history", qualifiedByName = "historyToHistoryDto")
    @Mapping(target = "requestUsers", source = "requestUsers", qualifiedByName = "requestUsersToRequestUsersDto")
    @Mapping(target = "photos", source = "photos", qualifiedByName = "photoToPhotoDto")
    @Mapping(target = "messages", source = "messages", qualifiedByName = "messageToMessageDto")
    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "usersToUsersDto")
    @Mapping(target = "admin", source = "admin.uid")
    public abstract ProjectLoggedInDTO projectToProjectDto(Project project);

    public abstract Collection<ProjectLoggedInDTO> projectToProjectDto(Collection<Project> projects);

    @Mapping(target = "history", source = "history", qualifiedByName = "historyDtoToHistory")
    @Mapping(target = "requestUsers", source = "requestUsers", qualifiedByName = "requestUsersDtoToRequestUsers")
    @Mapping(target = "photos", source = "photos", qualifiedByName = "photoDtoToPhoto")
    @Mapping(target = "messages", source = "messages", qualifiedByName = "messageDtoToMessage")
    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "UserDtoToUser")
    @Mapping(target = "admin", source = "admin", qualifiedByName = "AdminToId")
    public abstract Project projectDtoToProject(ProjectLoggedInDTO dto);

    public abstract Collection<Project> projectDtoToProject(Collection<ProjectLoggedInDTO> dtos);

    @Named("historyDtoToHistory")
    History historyDtoToHistory(HistoryProjectDTO dto){return historyService.findById(dto.getId());}
    @Named("requestUsersDtoToRequestUsers")
    LoggedInUser requestUserDtoToRequestUser(UserRequestDTO dto){return loggedInUserService.findById(dto.getUid());}
    @Named("AdminToId")
    LoggedInUser mapAdminIdToProject(String uid) {
        return loggedInUserService.findById(uid);
    }
    @Named("UserDtoToUser")
    LoggedInUser mapUserIdToProject(String uid){return loggedInUserService.findById(uid);}
    @Named("photoDtoToPhoto")
    Photo mapDtoToPhoto(PhotoDTO dto) {
        return photoService.findById(dto.getId());
    }
    @Named("messageDtoToMessage")
    Message mapDtoToMessage(MessageWithUserNameDTO dto) {
        return messageService.findById(dto.getId());
    }

    @Named("historyToHistoryDto")
    Set<HistoryProjectDTO> mapHistoryToDto(Set<History> source) {
        var dtos = new HashSet<HistoryProjectDTO>();
        source.forEach(e -> dtos.add(historyMapper.historyToHistoryDto(e)));
        return dtos;
    }
    @Named("requestUsersToRequestUsersDto")
    Set<UserRequestDTO> mapRequestUserToDto(Set<LoggedInUser> source) {
        var dtos = new HashSet<UserRequestDTO>();
        source.forEach(e -> dtos.add(userRequestMapper.userToUserDto(e)));
        return dtos;
    }
    @Named("messageToMessageDto")
    Set<MessageWithUserNameDTO> mapMessageToDto(Set<Message> source) {
        var dtos = new HashSet<MessageWithUserNameDTO>();
        source.forEach(e -> dtos.add(messageWithUserNameMapper.messageToMessageDTO(e)));
        return dtos;
    }
    @Named("photoToPhotoDto")
    Set<PhotoDTO> mapPhotoToDto(Set<Photo> source) {
        var dtos = new HashSet<PhotoDTO>();
        source.forEach(e -> dtos.add(photoMapper.photoToPhotoDto(e)));
        return dtos;
    }

    @Named("usersToUsersDto")
    Set<String> mapUsersToIds(Set<LoggedInUser> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getUid()).collect(Collectors.toSet());
    }
}