package com.example.legalt.mappers.project;

import com.example.legalt.model.LoggedInUser;
import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Project.ProjectChangeRequestDTO;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class ProjectChangeRequestMapper {

    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected LoggedInUserService loggedInUserService;

    @Mapping(target = "requestUsers", source = "requestUsers", qualifiedByName = "requestUsersToRequestUsersDto")
    public abstract ProjectChangeRequestDTO projectToProjectDto(Project project);

    public abstract Collection<ProjectChangeRequestDTO> projectToProjectDto(Collection<Project> projects);

    @Mapping(target = "requestUsers", source = "requestUsers", qualifiedByName = "requestUsersDtoToRequestUsers")
    public abstract Project projectDtoToProject(ProjectChangeRequestDTO dto);

    public abstract Collection<Project> projectDtoToProject(Collection<ProjectChangeRequestDTO> projectDTOS);

    @Named("requestUsersDtoToRequestUsers")
    LoggedInUser mapIdToProject(String uid){return loggedInUserService.findById(uid);}

    @Named("requestUsersToRequestUsersDto")
    Set<String> mapProjectsToIds(Set<LoggedInUser> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getUid()).collect(Collectors.toSet());
    }
}
