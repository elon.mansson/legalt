package com.example.legalt.mappers.project;

import com.example.legalt.model.Project;
import com.example.legalt.model.dtos.Project.UpdateProjectAdminDTO;
import org.mapstruct.Mapper;

import java.util.Collection;
@Mapper(componentModel = "spring")
public abstract class UpdateProjectAdminMapper {

    public abstract UpdateProjectAdminDTO projectToProjectDto(Project project);

    public abstract Collection<UpdateProjectAdminDTO> projectToProjectDto(Collection<Project> projects);

    public abstract Project projectDtoToProject(UpdateProjectAdminDTO dto);

    public abstract Collection<Project> projectDtoToProject(Collection<UpdateProjectAdminDTO> projectDTOS);
}
