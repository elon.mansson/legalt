package com.example.legalt.mappers.project;

import com.example.legalt.model.*;
import com.example.legalt.model.dtos.Project.CreateProjectDTO;
import com.example.legalt.model.dtos.Project.DeleteProjectDTO;
import com.example.legalt.service.History.HistoryService;
import com.example.legalt.service.LoggedInUser.LoggedInUserService;
import com.example.legalt.service.Message.MessageService;
import com.example.legalt.service.Photo.PhotoService;
import com.example.legalt.service.Project.ProjectService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class DeleteProjectMapper {

    @Autowired
    protected LoggedInUserService loggedInUserService;
    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected MessageService messageService;
    @Autowired
    protected PhotoService photoService;
    @Autowired
    protected HistoryService historyService;

    @Mapping(target = "history", source = "history", qualifiedByName = "historyToHistoryDto")
    @Mapping(target = "admin", source = "admin.uid")
    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "ProjectToProjectDto")
    @Mapping(target = "photos", source = "photos", qualifiedByName = "photosToPhotosDto")
    @Mapping(target = "messages", source = "messages", qualifiedByName = "messageToMessageDto")
    @Mapping(target = "requestUsers", source = "requestUsers", qualifiedByName = "requestUsersToRequestUsersDto")
    public abstract DeleteProjectDTO projectToProjectDto(Project project);

    public abstract Collection<DeleteProjectDTO> projectToProjectDto(Collection<Project> projects);

    @Mapping(target = "history", source = "history", qualifiedByName = "historyDtoToHistory")
    @Mapping(target = "admin", source = "admin", qualifiedByName = "AdminToId")
    @Mapping(target = "loggedInUsers", source = "loggedInUsers", qualifiedByName = "ProjectDtoToProject")
    @Mapping(target = "photos", source = "photos", qualifiedByName = "photosDtoToPhotos")
    @Mapping(target = "requestUsers", source = "requestUsers", qualifiedByName = "requestUsersDtoToRequestUsers")
    @Mapping(target = "messages", source = "messages", qualifiedByName = "messageDtoToMessage")
    public abstract Project projectDtoToProject(DeleteProjectDTO dto);

    @Mapping(target = "history", ignore = true)
    @Mapping(target = "photos", ignore = true)
    @Mapping(target = "requestUsers", ignore = true)
    @Mapping(target = "loggedInUsers", ignore = true)
    @Mapping(target = "admin", ignore = true)
    @Mapping(target = "messages", ignore = true)
    public abstract Project projectDtoToProject(CreateProjectDTO dto);

    public abstract Collection<Project> projectDtoToProject(Collection<DeleteProjectDTO> projectDTOS);

    @Named("photosDtoToPhotos")
    Photo mapPhotoIdToProject(int id){return photoService.findById(id);}
    @Named("historyDtoToHistory")
    History maphistoryIdToHistory(int id){return historyService.findById(id);}
    @Named("requestUsersDtoToRequestUsers")
    LoggedInUser mapRequestUserIdToProject(String uid){return loggedInUserService.findById(uid);}
    @Named("ProjectDtoToProject")
    LoggedInUser mapIdToProject(String uid){return loggedInUserService.findById(uid);}
    @Named("AdminToId")
    LoggedInUser mapAdminIdToProject(String uid) {
        return loggedInUserService.findById(uid);
    }
    @Named("messageDtoToMessage")
    Message mapMessageIdToProject(int id){return messageService.findById(id);}


    @Named("photosToPhotosDto")
    Set<Integer> mapPhotosToIds(Set<Photo> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
    @Named("historyToHistoryDto")
    Set<Integer> mapHistoryToIds(Set<History> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
    @Named("requestUsersToRequestUsersDto")
    Set<String> mapRequestUsersToIds(Set<LoggedInUser> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getUid()).collect(Collectors.toSet());
    }
    @Named("ProjectToProjectDto")
    Set<String> mapProjectsToIds(Set<LoggedInUser> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getUid()).collect(Collectors.toSet());
    }
    @Named("messageToMessageDto")
    Set<Integer> mapMessagesToIds(Set<Message> source) {
        if(source == null){
            return null;
        }
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }
}
